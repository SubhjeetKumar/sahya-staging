-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 08, 2019 at 10:49 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `saathi`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE `tbl_user` (
  `userId` int(11) NOT NULL,
  `userType` enum('admin','user','employee') DEFAULT 'user',
  `userUniqueId` varchar(55) DEFAULT NULL,
  `userName` varchar(255) DEFAULT '',
  `roleId` int(11) DEFAULT NULL,
  `password` varchar(255) DEFAULT '',
  `name` varchar(255) DEFAULT NULL,
  `nameAlias` varchar(255) DEFAULT NULL,
  `domainOfWork` varchar(255) DEFAULT NULL,
  `monthlyIncome` varchar(255) DEFAULT NULL,
  `noOfChildren` varchar(255) DEFAULT NULL,
  `male_children` varchar(255) DEFAULT NULL,
  `female_children` varchar(255) DEFAULT NULL,
  `total_children` varchar(255) DEFAULT NULL,
  `referralPoint` varchar(255) DEFAULT NULL,
  `referralPoint_others` varchar(255) DEFAULT NULL,
  `addressState` int(11) DEFAULT NULL,
  `addressDistrict` int(11) DEFAULT NULL,
  `address` text,
  `primaryIdentity` varchar(255) DEFAULT NULL,
  `primaryIdentity_others` varchar(255) DEFAULT NULL,
  `secondaryIdentity` varchar(255) DEFAULT NULL,
  `secondaryIdentity_other` varchar(255) DEFAULT NULL,
  `hivHistory` varchar(255) DEFAULT NULL,
  `gender` enum('Male','Female','TG') DEFAULT NULL,
  `emailAddress` varchar(255) DEFAULT '',
  `age` varchar(55) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `occupation` varchar(255) DEFAULT '',
  `occupation_other` varchar(255) DEFAULT NULL,
  `educationalLevel` varchar(255) DEFAULT '',
  `districtId` int(11) DEFAULT NULL,
  `state` int(11) DEFAULT NULL,
  `placeOforigin` varchar(255) DEFAULT '',
  `mobileNo` varchar(55) DEFAULT NULL,
  `maritalStatus` enum('Married','Divorced','Widow/Widower','Unmarried','Separated','Others') DEFAULT 'Unmarried',
  `maritalStatus_other` varchar(255) DEFAULT NULL,
  `sought` varchar(255) DEFAULT NULL,
  `condomUsage` varchar(255) DEFAULT NULL,
  `substanceUse` varchar(255) DEFAULT NULL,
  `multipleSexPartner` varchar(255) DEFAULT NULL,
  `prefferedSexualAct` varchar(255) DEFAULT NULL,
  `pastHivReport` varchar(255) DEFAULT NULL,
  `testHiv` varchar(255) DEFAULT NULL,
  `prefferedGender` varchar(255) DEFAULT NULL,
  `hivTestTime` varchar(255) DEFAULT NULL,
  `hivTestResult` varchar(255) DEFAULT NULL,
  `fingerDate` date DEFAULT NULL,
  `fingerReport` varchar(255) DEFAULT NULL,
  `saictcStatus` varchar(255) DEFAULT NULL,
  `saictcDate` date DEFAULT NULL,
  `saictcPlace` varchar(255) DEFAULT NULL,
  `ictcNumber` varchar(255) DEFAULT NULL,
  `hivDate` date DEFAULT NULL,
  `hivStatus` varchar(255) DEFAULT NULL,
  `reportIssuedDate` date DEFAULT NULL,
  `reportStatus` varchar(255) DEFAULT NULL,
  `campCode` varchar(255) DEFAULT NULL,
  `artCenter` varchar(255) DEFAULT NULL,
  `artNumber` varchar(255) DEFAULT NULL,
  `cd4status` varchar(255) DEFAULT NULL,
  `cd4Result` varchar(255) DEFAULT NULL,
  `artStatus` varchar(255) DEFAULT NULL,
  `syphilisTest` varchar(255) DEFAULT NULL,
  `syphilisResult` varchar(255) DEFAULT NULL,
  `tb_test` varchar(255) DEFAULT NULL,
  `tbResult` varchar(255) DEFAULT NULL,
  `rntcpRefer` varchar(255) DEFAULT NULL,
  `remark` varchar(255) DEFAULT NULL,
  `shareInfoAboutSexualBehaviour` varchar(255) DEFAULT NULL,
  `sexualBehaviour` varchar(255) DEFAULT '',
  `hydc` enum('Website','App') DEFAULT NULL,
  `registerFromDevice` enum('Web','App','Sms','Offline- One to One','Offline-Camps-CBS Events') DEFAULT NULL,
  `registerMode` enum('Online','Offline') DEFAULT NULL,
  `smsUser` enum('Y','N') DEFAULT 'N',
  `registeredBy` varchar(11) DEFAULT NULL,
  `registeredOn` date DEFAULT NULL,
  `registrationNumber` varchar(50) DEFAULT NULL,
  `modeOfContact` varchar(50) DEFAULT NULL,
  `hrg` varchar(50) DEFAULT NULL,
  `arg` varchar(50) DEFAULT NULL,
  `ictcUpload` varchar(200) DEFAULT NULL,
  `linkToArt` varchar(50) DEFAULT NULL,
  `artDate` varchar(50) DEFAULT NULL,
  `artUpload` varchar(50) DEFAULT NULL,
  `otherService` varchar(50) DEFAULT NULL,
  `clientStatus` varchar(50) DEFAULT NULL,
  `createdBy` int(11) DEFAULT NULL,
  `createdDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedDate` datetime DEFAULT NULL,
  `updatedBy` varchar(255) DEFAULT NULL,
  `otp` int(11) DEFAULT NULL,
  `userVerify` enum('Y','N') DEFAULT 'N',
  `deleted` enum('Y','N') DEFAULT 'N',
  `agreeSms` enum('Y','N') DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`userId`, `userType`, `userUniqueId`, `userName`, `roleId`, `password`, `name`, `nameAlias`, `domainOfWork`, `monthlyIncome`, `noOfChildren`, `male_children`, `female_children`, `total_children`, `referralPoint`, `referralPoint_others`, `addressState`, `addressDistrict`, `address`, `primaryIdentity`, `primaryIdentity_others`, `secondaryIdentity`, `secondaryIdentity_other`, `hivHistory`, `gender`, `emailAddress`, `age`, `dob`, `occupation`, `occupation_other`, `educationalLevel`, `districtId`, `state`, `placeOforigin`, `mobileNo`, `maritalStatus`, `maritalStatus_other`, `sought`, `condomUsage`, `substanceUse`, `multipleSexPartner`, `prefferedSexualAct`, `pastHivReport`, `testHiv`, `prefferedGender`, `hivTestTime`, `hivTestResult`, `fingerDate`, `fingerReport`, `saictcStatus`, `saictcDate`, `saictcPlace`, `ictcNumber`, `hivDate`, `hivStatus`, `reportIssuedDate`, `reportStatus`, `campCode`, `artCenter`, `artNumber`, `cd4status`, `cd4Result`, `artStatus`, `syphilisTest`, `syphilisResult`, `tb_test`, `tbResult`, `rntcpRefer`, `remark`, `shareInfoAboutSexualBehaviour`, `sexualBehaviour`, `hydc`, `registerFromDevice`, `registerMode`, `smsUser`, `registeredBy`, `registeredOn`, `registrationNumber`, `modeOfContact`, `hrg`, `arg`, `ictcUpload`, `linkToArt`, `artDate`, `artUpload`, `otherService`, `clientStatus`, `createdBy`, `createdDate`, `updatedDate`, `updatedBy`, `otp`, `userVerify`, `deleted`, `agreeSms`) VALUES
(1, 'user', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, '', NULL, NULL, '', '+919654529341', 'Unmarried', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Sms', NULL, 'N', NULL, NULL, '', '', '', '', '', '', '', '', '', '', NULL, '2018-10-31 13:35:44', NULL, NULL, NULL, 'N', 'Y', 'Y'),
(2, 'user', 'A1HRFR00001', 'shikha', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'shikha', '', NULL, '', NULL, '', '', '', NULL, NULL, 13, 176, 'sec-3 Faridabad', NULL, NULL, '', '0', NULL, '', '', '22', '1995-11-30', '0', '0', '0', 0, 0, '', '+919654529341', '', '0', '', '', '', '0', '', '', '', '', NULL, '', '0000-00-00', NULL, '', '0000-00-00', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 'Sms', 'Online', 'Y', '', '1970-01-01', '', '', '', '', '', '', '', '', '', '', 1, '2018-10-31 13:45:50', NULL, NULL, NULL, 'Y', 'Y', 'Y'),
(3, 'user', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, '', NULL, NULL, '', '+919810754321', 'Unmarried', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Sms', NULL, 'N', NULL, NULL, '', '', '', '', '', '', '', '', '', '', NULL, '2018-10-31 14:35:25', NULL, NULL, NULL, 'N', 'Y', 'Y'),
(4, 'admin', NULL, 'admin', NULL, '123456', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '', NULL, '', NULL, NULL, '', NULL, 'Unmarried', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, 'N', NULL, NULL, '', '', '', '', '', '', '', '', '', '', NULL, '2018-10-31 16:20:02', NULL, NULL, NULL, 'Y', 'N', 'N'),
(5, 'user', 'A1DLCD00001', 'ksen31Oct', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'Kamalika', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 10, 137, 'Delhi', NULL, NULL, '', '0', NULL, 'Female', '', '23', '1995-06-14', '0', '0', '0', 0, 0, '', '+919818438353', '', '0', '', '', '', '', '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 'Sms', 'Online', 'Y', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 4, '2018-10-31 18:34:41', NULL, NULL, 26167, 'Y', 'N', 'Y'),
(6, 'user', 'A1BRPA00001', '+918801289261', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'Vikash', '', NULL, '', NULL, '', '', '', NULL, NULL, 5, 61, 'sec-37 Faridabad', NULL, NULL, '', '0', NULL, '', '', '25', '1993-01-20', '0', '0', '0', 0, 0, '', '+918801289261', '', '0', '', '', '', '0', '', '', '', '', NULL, '', '0000-00-00', NULL, '', '0000-00-00', '', '', '0000-00-00', '', '0000-00-00', '', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 'Web', 'Online', 'N', '', '1970-01-01', '', '', '', '', '', '', '', '', '', '', 4, '2018-11-01 05:03:49', NULL, NULL, NULL, 'Y', 'N', 'N'),
(7, 'employee', NULL, 'test_userDM', 3, '123456', 'Test User Data Manager', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'kamalika@saathii.org', NULL, NULL, '', NULL, '', NULL, NULL, '', '9818438353', 'Unmarried', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Web', 'Online', 'N', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 4, '2018-12-01 11:10:51', NULL, NULL, 51092, 'Y', 'N', 'N'),
(8, 'employee', NULL, 'test_dm_admin', 10, '123456', 'Test Data Manager Admin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@saathii.org', NULL, NULL, '', NULL, '', NULL, NULL, '', '9818438353', 'Unmarried', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Web', 'Online', 'N', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 4, '2018-12-17 10:15:38', '2018-12-17 15:47:32', '4', NULL, 'Y', 'N', 'N'),
(9, 'employee', NULL, 'emp_4_jan', 11, '123456', 'test employee1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test@gmail.com', NULL, NULL, '', NULL, '', NULL, NULL, '', '9999999999', 'Unmarried', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, 'Web', 'Online', 'N', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 4, '2019-01-04 06:04:50', '2019-01-04 11:35:02', '4', NULL, 'Y', 'N', 'N'),
(10, 'user', 'A1HRFR00002', 'shikha', NULL, 'e10adc3949ba59abbe56e057f20f883e', 'shikha', 'shikha', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 13, 176, 'sector-3', NULL, NULL, '', '0', NULL, 'Female', '', '23', '1995-11-30', '0', '0', '0', 0, 0, '', '+919654529341', '', '0', '', '', '', '', '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, 'Sms', 'Online', 'Y', NULL, NULL, '', '', '', '', '', '', '', '', '', '', 0, '2019-01-21 13:27:17', NULL, NULL, 80126, 'Y', 'N', 'Y'),
(11, 'user', '101', '9090909909', NULL, 'd41d8cd98f00b204e9800998ecf8427e', 'scannerf', 'sc', NULL, '>1000', NULL, '3', '1', '4', 'Hotspot', 'Refarral other', 3, 19, 'sec-6', NULL, NULL, NULL, NULL, NULL, 'Male', '', '28', '1990-06-06', 'Self employed', 'occupation 1', 'Primary(1-5)', 1, 1, '', '+919090909909', 'Widow/Widower', 'Marital Other', 'Yes', 'In every sex', 'Tabcoo', 'Yes', 'Oral,Vaginal', 'Reactive', 'Yes', 'Female', 'Yes', 'Yes', '2019-02-27', NULL, 'Yes', NULL, '', '', NULL, '', NULL, '-select', '24', NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'bjbhjb', NULL, 'Yes', NULL, 'Web', 'Online', 'N', 'CRP', '2019-02-14', '23124', '', 'TG(F-M)', 'Single Male migrant', NULL, '', NULL, NULL, NULL, '', 4, '2019-02-07 12:52:24', NULL, '4', NULL, 'Y', 'N', 'N');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
  ADD PRIMARY KEY (`userId`),
  ADD KEY `fk_districtId_idx` (`districtId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
