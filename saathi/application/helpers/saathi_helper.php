<?php
 if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('changeDateMDY'))
{
	function changeDateMDY($date)
	{
		$d=explode("/",$date);
		$s =$d[1]."/".$d[0]."/".$d[2];
		//echo $s;exit();
		return $s;
	}
}


if(!function_exists('checkarray'))
{
	function checkarray($data){
		if(strpos($data,',',0)!=false){
			$a=explode(",",$data);
		}else{
			if($data<4){
			$a=[$data];
			}
			else{
				$a=[];
			}
		}
		return $a;
	}
}

if(! function_exists('prefferedGender'))
{
	function prefferedGender($data)
	{
		$as=array(1=>"Male",2=>"Female",3=>"TG");
		return $as[$data];
	}
}

if(! function_exists('prefferedSexualAct'))
{
	function prefferedSexualAct($data)
	{
		$as=array(1=>"Oral",2=>"Anal",3=>"Vaginal");
		return $as[$data];
	}
}

if(! function_exists('substanceUse'))
{
	function substanceUse($data)
	{
		$as=array(1=>"Tobacco",2=>"Drug",3=>"Alcohol");
		return $as[$data];
	}
}

if(! function_exists('otherService'))
{
	function otherService($data)
	 {

	 	$as=array(1=>"Positive living counselling",2=>"ART adherence counselling",3=>"Linkage to Social protection",4=>"Other Services");
	 	return $as[$data];
	}
}

if(! function_exists('constrinexcel'))
{
	function constrinexcel($data)
	{//trim element of array
		for($i=0;$i<sizeof($data);$i++)
		{
    		$ar[$i]=trim($data[$i]);
  		}
 		return $ar;
	}
}

if(! function_exists('isSubArray'))
{
	function isSubArray($a,$b)
	{//check array a is subarray of array b or not 
			$t=true;
			  if(sizeof($a)<=sizeof($b))
			  {
			    for($i=0;$i<sizeof($a);$i++)
			    {
			       if(!(in_array($a[$i],$b)))
			       {
			          $t= false;			          
			       }
			       else
			       {
			              $t=true;
			       }
			    }
			  } 
			  else
			  {
			    $t=false; 
			  }
			
			  return $t;
	}
}


?>