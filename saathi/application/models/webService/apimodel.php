<?php

class ApiModel extends CI_Model {

	function __construct(){
	 	parent::__construct();
	 	   $CI = &get_instance();
        $this->db2 = $CI->load->database('db2', TRUE);
	 }


    public function login($data) {
		$post['p_userName']	 = $data['userName'];	
		$post['p_password']	 = md5($data['password']);
		$post['p_quizUniqueNumber']	 = $data['quizUniqueNumber'];
		$post['p_loginTime']	 = $data['loginTime'];
		$stored = "Call proc_login_validation_app(?,?,?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();			
		return $result;
    }
	
	public function searchServiceProvider($data) {
		$post['p_searchText']	 = $data['searchText'];	
		$post['p_serviceTypeId']	 = $data['serviceTypeId'];	
		$post['p_serviceTypeParameterId']	 = $data['serviceTypeParameterId'];	
		$post['p_latLong']	 = $data['latLong'];	
		$stored = "Call proc_service_provider_search(?,?,?,?)";
		$query = $this->db->query($stored,$post);
		//echo $this->db->last_query();
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
		//echo '<pre>';print_r($result);exit;
		if($post['p_latLong'] == ''){
			return $result;
		}else{
			foreach($result as $value){
				//echo $value['geoLocation'];
				$geoFrom = $post['p_latLong'];
				$geoTo = $value['latitude'].','.$value['longitude'];
				$distance = $this->getDistance($geoFrom, $geoTo, "K");
				//print_r($distance);
				if($distance['range']<= '5'){
					$rr[]=$value;
				}
				$i++;
			}
			return $rr;
		}
	}
	
	public function getDistance($geoFrom, $geoTo, $unit){
		$data1= explode(',',$geoFrom);
		$data2= explode(',',$geoTo);
		$latitudeFrom = $data1[0];
		$longitudeFrom = $data1[1];
		$latitudeTo = $data2[0];
		$longitudeTo = $data2[1];
		
		//Calculate distance from latitude and longitude
		$theta = $longitudeFrom - $longitudeTo;
		$dist = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
		$dist = acos($dist);
		$dist = rad2deg($dist);
		$miles = $dist * 60 * 1.1515;
		$unit = strtoupper($unit);
		if ($unit == "K") {
			$range = ($miles * 1.609344);
		} else if ($unit == "N") {
			$range = ($miles * 0.8684);
		} else {
			$range = $miles;
		}
		$geo = $latitudeTo.','.$longitudeTo;
		$result = array('range'=>$range);
		return $result;
	}
	
	public function registerUser($data) {
		//echo '<pre>';print_r($data);exit;
		$post['p_nameAlias']	 = $data['name'];	
		$post['p_age']	 = $data['age'];
		$post['p_dob']	 = $data['dob'];
		$post['p_gender']	 = $data['gender'];
		$post['p_email']	 = $data['email'];
		$post['p_occupation']	 = $data['occupation'];
		$post['p_educationLevel']	 = $data['educationLevel'];
		$post['p_userName']	 = $data['userName'];
		$post['p_password']	 = $data['password'];
		$post['p_district']	 = $data['district'];
		$post['p_state']	 = $data['state'];
		$post['p_placeofOrigin']	 = $data['placeofOrigin'];
		$post['p_mobile']	 = $data['mobile'];
		$post['p_maritalStatus']	 = $data['maritalStatus'];
		$post['p_behaviour']	 = $data['behaviour'];
		$post['p_hydc']	 = $data['hydc'];
		//echo '<pre>';print_r($post);exit;
		$stored = "Call proc_register_user_app(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
			
		return $result;
    }
	
	public function forgotPassword($data) {
		$post['p_userName']	 = $data['userName'];	
		$stored = "Call proc_forgot_password(?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
			
		return $result;
    }
	
	public function changePassword($data) {
		$post['p_userId']	 = $data['userId'];	
		$post['p_oldPassword']	 = $data['oldPassword'];	
		$post['p_newPassword']	 = $data['newPassword'];	
		$stored = "Call proc_change_password(?,?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
			
		return $result;
    }
	
	public function eventsList($data) {
		$post['p_eventId']	 = $data['eventId'];	
		$stored = "Call proc_event_data(?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();	
		$query->free_result();
		$query->next_result();
		
		return $result;
    }
	
	public function ongroundPartnersList($data) {
		if($data == NULL)
		{
           $post['p_ongroundPartnerId'] = '';
		}
		else
	   {	
		$post['p_ongroundPartnerId']	 = $data;	
	   }	
		$stored = "Call proc_onground_partner_data(?)";
		$query = $this->db->query($stored,$post);
		//echo $this->db->last_query(); exit;
			
		$result = $query->result_array();	
       
     	$query->free_result();
		$query->next_result();	
		return $result;
    }
	
	public function vouchersList($data) {
		/*$post['p_userId']	 = $data['userId'];	
		$post['p_voucherId']	 = $data['voucherId'];	
		$stored = "Call proc_voucher_data(?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();


       //  print_r($result);exit();
       
		$query->free_result();
		$query->next_result();	
		
		return $result;*/

		$sql = "SELECT 'Voucher Detail Fetched Successfully' AS responseMessage,'200' AS responseCode,t1.voucherId,t1.voucherNumber,t1.voucherCode,DATE_FORMAT(t1.voucherDate,'%d-%m-%Y') AS voucherDate,
			DATE_FORMAT(t1.voucherExpDate,'%d-%m-%Y') AS voucherExpDate,t2.voucherBackName,
			CASE WHEN t2.voucherBackName = 'service'
			THEN
			'Service Access Voucher'
			WHEN t2.voucherBackName = 'game'
			THEN
			'Game Voucher'
			ELSE
			'Quiz Voucher'
			END
			AS 
			voucherType,
			CASE WHEN t2.voucherBackName = 'service'
			THEN
			(SELECT `name` FROM `tbl_service_provider_details` WHERE serviceProviderId = t1.categoryId)
			WHEN t2.voucherBackName = 'game'
			THEN
			(SELECT `gameName` FROM `tbl_game_master` WHERE id = t1.categoryId)
			ELSE
			(SELECT `quizName` FROM `tbl_quiz_names` WHERE quizId = t1.categoryId)
			END
			AS 
			categoryName,
			CASE WHEN t2.voucherBackName = 'service'
			THEN
			(SELECT `latitude` FROM `tbl_service_provider_details` WHERE serviceProviderId = t1.categoryId)
			WHEN t2.voucherBackName = 'game'
			THEN
			''
			ELSE
			''
			END
			AS 
			latitude,
			CASE WHEN t2.voucherBackName = 'service'
			THEN
			(SELECT `longitude` FROM `tbl_service_provider_details` WHERE serviceProviderId = t1.categoryId)
			WHEN t2.voucherBackName = 'game'
			THEN
			''
			ELSE
			''
			END
			AS 
			longitude,
			CASE WHEN t2.voucherBackName = 'service'
			THEN
			(SELECT `address` FROM `tbl_service_provider_details` WHERE serviceProviderId = t1.categoryId)
			WHEN t2.voucherBackName = 'game'
			THEN
			''
			ELSE
			''
			END
			AS 
			address,
			CASE WHEN t2.voucherBackName = 'service'
			THEN
			(SELECT `mobile` FROM `tbl_service_provider_details` WHERE serviceProviderId = t1.categoryId)
			WHEN t2.voucherBackName = 'game'
			THEN
			''
			ELSE
			''
			END
			AS 
			mobile,
			CASE WHEN t2.voucherBackName = 'service'
			THEN
			(SELECT `officePhone` FROM `tbl_service_provider_details` WHERE serviceProviderId = t1.categoryId)
			WHEN t2.voucherBackName = 'game'
			THEN
			''
			ELSE
			''
			END
			AS 
			officePhone
			FROM `tbl_voucher_creation_data` AS t1 
			LEFT JOIN `tbl_voucher_type` AS t2 ON t1.voucherTypeId = t2.voucherTypeId
			WHERE t1.userId = '".$data['userId']."' AND t1.deleted = 'N';";

			$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;		

		
    }
	
	public function otpVerification($data) {
		$post['p_userId']	 = $data['userId'];	
		$post['p_otp']	 = $data['otp'];
		$post['p_quizUniqueNumber']	 = $data['quizUniqueNumber'];		
		$stored = "Call proc_otp_verification(?,?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
			
		return $result;
    }
	
	public function serviceAccessVoucher($data) {
		$post['p_userId']	 = $data['userId'];	
		$post['p_serviceProviderId']	 = $data['serviceProviderId'];
		$post['p_voucherCode'] = $data['voucherCode'];	
		$stored = "Call proc_service_access_voucher(?,?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();	

		$query->next_result(); 
        $query->free_result(); 
 
		return $result;
    }
	
	public function playedQuizList($data) {
		$post['p_userId']	 = $data['userId'];	
		$stored = "Call proc_played_quiz_list(?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();	
		$query->free_result();
		$query->next_result();
		
		return $result;
    }
	
	public function quizList() {
		$str=base_url()."/uploads/quizImage/";
		$sql="SELECT quizId,quizName,concat('".$str."',quizImage) quizImage FROM `tbl_quiz_names` 
				WHERE deleted = 'N' 
				AND quizId IN (SELECT quizId FROM `tbl_quiz_questions` WHERE deleted = 'N') ORDER BY createdDate DESC";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;	
    }
	
	public function newQuizQuestions($data) {
		$sql="SELECT quizQuestionId,quizQuestionName,typeOfAnswer FROM `tbl_quiz_questions` WHERE quizId = ".$data['quizId']." AND deleted = 'N' AND quizQuestionName != '' ";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		foreach($result as $val){
			$result1 = $this->newQuizQuestionOptions($val);
			
			//echo '<pre>';print_r($result1);
			$res['options'] = $result1;
			$cc = $val;
			$aa[] = array_merge($cc,$res);
		}
		
		//echo '<pre>';print_r($aa);
		return $aa;
	}
	
	public function newQuizQuestionOptions($data) {
		$sql1="SELECT quizQuestionOptionId as optionId,quizQuestionOptionName as optionName FROM `tbl_quiz_question_options` WHERE quizQuestionId = ".$data['quizQuestionId']." AND deleted = 'N' ";
		$query1 = $this->db->query($sql1);
		//echo $this->db->last_query();
		$result1 = $query1->result_array();
		foreach($result1 as $val){
			$option[] = $val;
		}
		return $option;
	}
	
	public function eventSearch($data) {
		$post['p_name']	 = $data['name'];	
		$post['p_date']	 = $data['date'];	
		$stored = "Call proc_event_search(?,?)";
		$query = $this->db->query($stored,$post);
		//echo $this->db->last_query();
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
			
		return $result;
	}
	
	public function stateList() {
		$sql="SELECT stateId,stateName,stateCode FROM `tbl_state` WHERE deleted = 'N'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;	
    }
	
	public function districtList($data) {
		$sql="SELECT districtId,districtName,districtCode,stateId FROM `tbl_district` WHERE deleted = 'N' AND stateId = ".$data['stateId']."";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;	
    }
	
	public function addServiceProviderLocation($data) {
		$post['p_userId']	 = $data['userId'];	
		$post['p_serviceProviderId']	 = $data['serviceProviderId'];	
		$post['p_lat']	 = $data['lat'];	
		$post['p_long']	 = $data['long'];	
		$stored = "Call proc_add_service_provider_location(?,?,?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
			
		return $result;
    }
	
	public function menuList($data) {
		$sql="SELECT t1.menuId,t2.title,t2.component,t2.icon FROM `tbl_menu_app_mapping` AS t1 
				LEFT JOIN `tbl_menu_app` AS t2 ON t1.menuId = t2.menuId
				WHERE t1.userType = '".$data['userType']."' AND t1.deleted = 'N' AND t2.deleted = 'N'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;	
    }
	
	public function serviceProviderType() {
		$sql="SELECT serviceTypeId,serviceTypeName FROM `tbl_service_type` WHERE deleted ='N'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;	
    }
	
	public function submitQuiz($data) {
		$post['p_quizId']	 = $data['quizId'];	
		$post['p_quizStartTime']	 = $data['quizStartTime'];	
		$post['p_quizEndTime']	 = $data['quizEndTime'];	
		$post['p_quizQuestionId']	 = $data['quizQuestionId'];	
		$post['p_quizQuestionOptionId']	 = $data['quizQuestionOptionId'];	
		$post['p_userId']	 = $data['userId'];	
		$stored = "Call proc_submit_quiz(?,?,?,?,?,?)";
		$query = $this->db->query($stored,$post);
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
		$sql1="SELECT t2.quizQuestionName,GROUP_CONCAT(t3.quizQuestionOptionName) as answer,
				(SELECT GROUP_CONCAT(quizQuestionOptionName) FROM tbl_quiz_question_options WHERE quizQuestionId = t1.quizQuestionId AND quizQuestionAnswer = '1' AND deleted = 'N') as correctAnswer,
				CASE WHEN 
				(SELECT quizQuestionAnswer FROM tbl_quiz_question_options 
				WHERE quizQuestionOptionId = t1.quizQuestionOptionId) = '1'
				THEN
				'Y'
				ELSE
				'N'
				END
				AS anwserMode
				FROM `tbl_quiz_question_result` AS t1 
				LEFT JOIN `tbl_quiz_questions` AS t2 ON t1.quizQuestionId = t2.quizQuestionId
				LEFT JOIN `tbl_quiz_question_options` AS t3 ON t1.quizQuestionOptionId = t3.quizQuestionOptionId
				WHERE t1.quizUniqueNumber = '".$result[0]['quizUniqueNumber']."' AND t1.deleted = 'N' AND t2.deleted = 'N' AND t3.deleted = 'N' GROUP BY t2.quizQuestionName  ORDER BY t2.quizQuestionId";
		$query1 = $this->db->query($sql1);
		$result1 = $query1->result_array();
		 if(!empty($result1))
	       {	
		   foreach($result1 as $val1){
			$aa[] = $val1;
	       }
		$cc['data'] = $aa;
	      }
	   else
	   {
              $cc = array();
	   }
	   $dd = array_merge($result[0],$cc);	
		return $dd;

    }
	
	public function feedback($data){
		$post['p_userId']	 = $data['userId'];	
		$post['p_serviceProviderId']	 = $data['serviceProviderId'];	
		$post['p_feedback']	 = $data['feedback'];	
		$stored = "Call proc_feedback_submit(?,?,?)";
		$query = $this->db->query($stored,$post);
		//echo $this->db->last_query();
		$result = $query->result_array();	
		$query->free_result();
		$query->next_result();
		
		return $result;
	}
	
	public function sendSms($data){
		//print_r($data);exit;
		$mobile = $data['mobileNo'];
		$smsContent = str_replace(' ','+',$data['smsContent']);
		//$smsTime = $data['smsTime'];
		//$date = date('d-m-Y').'T'.date('h:i:s');
		$smsApi = 'http://smsw.co.in/API/WebSMS/Http/v1.0a/index.php?username=codecube&password=cod123&sender=CODECB&to='.$mobile.'&message='.$smsContent.'&reqid=1&format={json|text}&route_id=route+id&callback=Any+Callback+URL&unique=0&sendondate=';
		//echo $smsApi; //exit;
		$ch = curl_init($smsApi);
		//curl_setopt($ch, CURLOPT_BUFFERSIZE, 8400000);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $smsContent);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mobile);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $smsTime);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain')); 
		$result2 = curl_exec($ch); // This is the result from the API
		//print_r($result2);exit;
		curl_close($ch);
		//return $result2;
	}
	
	public function logout($data){
		$sql  = "INSERT INTO `tbl_login_logout_logs` (logType,userId,logTime,createdDate)
				VALUES('logout','".$data['userId']."','".$data['logoutTime']."',NOW())";
		$query = $this->db->query($sql);
		return true;
	}
	
	public function serivceTypeParameters($data) {
		$sql="SELECT t2.serviceTypeParameterId,t2.serviceTypeParameterName FROM
				`tbl_servicetype_parameter_mapping` AS t1 
				LEFT JOIN `tbl_service_type_parameters` AS t2 
				ON t1.serviceTypeParameterId = t2.serviceTypeParameterId
				WHERE serviceTypeId = '".$data['serviceTypeId']."' AND t1.deleted = 'N' AND t2.deleted = 'N'";
		$query = $this->db->query($sql);
		$result = $query->result_array();
		return $result;	
    }

    public function userRegistrationConsent($data)
    {
        date_default_timezone_set("Asia/Kolkata");

        $smsData['message'] = $data['smsContent'];

        $smsData['mobile'] = $data['mobile'];

        $this->db->insert('tbl_sms_data',$smsData);

        $data['mobile'] = '+91'.$data['mobile'];

        $data['smsContent'] = strtolower($data['smsContent']);

       if($data['smsContent'] == 'consent')
       {

      

          $mobile = substr($data['mobile'],3); 

       	 $this->db->select('userId,userUniqueId');

       	 $this->db->from('tbl_user');

       	$this->db->where(['deleted'=>'N','userVerify'=>'Y']);

       	 $this->db->where('mobileNo',$data['mobile']);

       	 $this->db->or_where('mobileNo',$mobile);


       	 $query = $this->db->get();

       	 $result = $query->result_array();

       	

       	 $query2 =  $this->db->get_where('tbl_sms_user',['deleted'=>'N','mobileNo'=>$data['mobile']]);

        $result2 = $query2->result_array();

    
       	 if(!empty($result) && empty($result2))
       	 {
       	 	

             $this->db->where('userId',$result[0]['userId']);

             $this->db->update('tbl_user',['agreeSms'=>'Y']);

       	 	$message = "Thank You for your consent to interact with Sahay.You can send STOP any time to discontinue this service";

       	 	$mobileNo = $result[0]['mobileNo'];
  
           $array = ['client_id'=>$result[0]['userUniqueId'],'mobileNo'=>$data['mobile'],'latestConsentConfirm'=>date('Y-m-d H:i:s'),'webUser'=>'Y'];

    	  $this->db->insert('tbl_sms_user',$array);

       	  }elseif (!empty($result2) && empty($result)) {
       	  
       	  	$this->db->where('id',$result2[0]['id']);

               $this->db->update('tbl_sms_user',['latestConsentConfirm'=>date('Y-m-d H:i:s'),'current_status'=>'CONSENTED']);

            $data2['mobileNo'] = $data['mobile'];

            $data2['registerFromDevice'] = 'Sms';

            $this->db->insert('tbl_user',$data2);  

            	$message = "Thank You for your consent to interact with Sahay.You can send STOP any time to discontinue this service";

       	 	$mobileNo = $data['mobile']; 

       	  }elseif (!empty($result2) && !empty($result)) {

       	  
       	  	 $this->db->where('userId',$result[0]['userId']);

             $this->db->update('tbl_user',['agreeSms'=>'Y']);

              	$this->db->where('id',$result2[0]['id']);

               $this->db->update('tbl_sms_user',['latestConsentConfirm'=>date('Y-m-d H:i:s'),'current_status'=>'CONSENTED']);

               	$message = "Thank You for your consent to interact with Sahay.You can send STOP any time to discontinue this service";

       	 	$mobileNo = $data['mobile'];

       	  }
       	 else{
       	 	
       	      $mobileNo = $data['mobile'];

       	      $message = "Thank You. Type CONFIRM and send to 9664964444 to consent to our services over SMS/call, and that you are above 18 years of age";

       	      $this->registerUserSms($data);

          }	
	
       }

       if($data['smsContent'] == 'confirm')
       {

       
          $this->db->select('userId');

       	 $this->db->from('tbl_user');

       	 $this->db->where('mobileNo',$data['mobile']);

       	 $this->db->where('deleted','N');

       	 $this->db->where('userVerify','Y');

       	 $query = $this->db->get();

       	 $result = $query->result_array();


       	  $this->db->select('id');

       	 $this->db->from('tbl_sms_user');

       	 $this->db->where('mobileNo',$data['mobile']);

       	 $this->db->where('deleted','N');

       	 $query1 = $this->db->get();

       	 $result1 = $query1->result_array();

       	 $mobileNo = $data['mobile'];

       	 if(!empty($result) && empty($result1))
       	 {
       	 	
       	 	$message = "To avail of Sahay SMS service, send CONSENT to 9664964444.";
       	 }
       	elseif(!empty($result1)){
       		
                $message = "Thank You for your consent to interact with Sahay.You can send STOP any time to discontinue this service";

               $this->db->where('id',$result1[0]['id']);

               $this->db->update('tbl_sms_user',['latestConsentConfirm'=>date('Y-m-d H:i:s'),'current_status'=>'CONSENTED']);
       	 	}else{
       	 		$message = "INVALID INPUT";
       	 	}	

         
       }	

       if($data['smsContent'] == 'stop')
       {
       	  $mobile = substr($data['mobile'],3); 
       		
       	  $this->db->select('mobileNo,userId');

       	  $this->db->from('tbl_user');

       	  $this->db->where('mobileNo',$data['mobile']);

       	  $this->db->or_where('mobileNo',$mobile);

       	  $query1 = $this->db->get();

       	  $result1 = $query1->result_array();


       	  $this->db->select('id,mobileNo');

       	 $this->db->from('tbl_sms_user');

       	 $this->db->where('mobileNo',$data['mobile']);

       	 $query2 = $this->db->get();

       	 $result2 = $query2->result_array();

       	  if(!empty($result1))
       	  {
       	  		
             $this->db->where('userId',$result1[0]['userId']);

             $this->db->update('tbl_user',['agreeSms'=>'N']); 
            
       	  	 $mobileNo = $result1[0]['mobileNo'];

       	  	 $message = "Your services for SMS has been stopped";
       	  }

       	  if(!empty($result2))
       	  {
       	  		
       	  	 $this->db->where('id',$result2[0]['id']);

       	  	 $this->db->update('tbl_sms_user',['current_status'=>'STOPPED','latestStopRequest'=>date('Y-m-d H:i:s')]);

       	  	 $mobileNo = $result2[0]['mobileNo'];

       	  	 $message = "Your services for SMS has been stopped";
       	  }

       	  if (empty($result1) && empty($result2)) {
       	  		
       	  		$mobileNo = $data['mobile'];

       	      $message = 'INVALID INPUT';
       	  	}	
       }

       if($data['smsContent'] != 'stop' && $data['smsContent'] != 'consent' && $data['smsContent'] != 'confirm')
       {
       	 
       	  $mobileNo = $data['mobile'];

       	  $message = 'INVALID INPUT';
       }

       $array = ['smsContent'=>$message,'mobile'=>$mobileNo];

       	$this->db->insert('tbl_sms_communication',$array);	


       	$mobile = $mobileNo;
		$smsContent = str_replace(' ','+',$message);
		//$smsTime = $data['smsTime'];
		//$date = date('d-m-Y').'T'.date('h:i:s');
		$smsApi = 'http://smsw.co.in/API/WebSMS/Http/v1.0a/index.php?username=codecube&password=cod123&sender=CODECB&to='.$mobile.'&message='.$smsContent.'&reqid=1&format={json|text}&route_id=route+id&callback=Any+Callback+URL&unique=0&sendondate=';
		//echo $smsApi; //exit;
		$ch = curl_init($smsApi);
		//curl_setopt($ch, CURLOPT_BUFFERSIZE, 8400000);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $smsContent);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mobile);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $smsTime);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain')); 
		$result2 = curl_exec($ch); // This is the result from the API
		//print_r($result2);exit;
		curl_close($ch);

    }


    public function registerUserSms($data)
    {
    	 date_default_timezone_set("Asia/Kolkata");

    	 $sql = "SELECT CONCAT('SMS',IFNULL(RIGHT(CONCAT('0000',MAX(SUBSTR(client_id,4))+1),5),'00001')) AS client_id FROM `tbl_sms_user` WHERE LEFT(`client_id`,3) = 'SMS' ";

    	 $query = $this->db->query($sql);
		$result = $query->result_array();


    	$array = ['client_id'=>$result[0]['client_id'],'mobileNo'=>$data['mobile'],'latestConsentConfirm'=>date('Y-m-d H:i:s')];

    	$this->db->insert('tbl_sms_user',$array);
    }

    
    public function campReportUniqueId()
    {
        $sql = "SELECT CONCAT('CMP',IFNULL(RIGHT(CONCAT('0000',MAX(SUBSTR(camp_code_unique_id,4))+1),5),'00001')) AS campReportUniqueId FROM `tbl_camp_reports` WHERE LEFT(camp_code_unique_id,3) = 'CMP'";

        $query = $this->db->query($sql);

        $result = $query->result_array();

        return $result[0]['campReportUniqueId'];
    } 

//15-06-2019
   public function searchServiceProviders($data) 
	{
		$post['p_stateId']	 = $data['stateId'];	
		$post['p_districtId']	 = $data['districtId'];	
		$post['p_serviceTypeId']	 = $data['serviceTypeId'];	
		$post['p_serviceTypeParameterId']	 = $data['serviceTypeParameterId'];	
		$post['p_latLong']	 = $data['latLong'];
		//print_r($post); exit;		
		$stored = "Call proc_service_provider_search_web(?,?,?,?,?)";
		$query = $this->db->query($stored,$post);
		//echo $this->db->last_query(); //exit;
		$result = $query->result_array();
		$query->free_result();
		$query->next_result();
		//echo '<pre>';print_r($result);exit;
	
		if($post['p_latLong'] == ''){
			return $result;
		}else{
			foreach($result as $value){
				//echo $value['geoLocation'];
				$geoFrom = $post['p_latLong'];
				$newLatitude = DMStoDDconvertor($value['latitude']);
				$newLongitude = DMStoDDconvertor($value['longitude']);
				$geoTo = $newLatitude.','.$newLongitude;

				$distance = $this->getDistance($geoFrom, $geoTo, "K");
				//print_r($distance);
				if($distance['range']<= 10){
					$rr[]=$value;
				}
				//$i++;
			}
		    if(!empty($rr))
			{
			return $rr;
		   }

		}
	}
	


	public function eventList($type) 
	{	
		$str=base_url()."/uploads/eventImage/";
		$sql="SELECT eventId,eventName,eventVenue,
			DATE_FORMAT(eventDate,'%d-%b-%Y')eventDate,
			concat('".$str."',IFNULL(NULLIF(eventImage,''),'dummy_image.jpg'))eventImage,
			startDate,startTime,endDate,endTime,otherInfo,website 
			FROM `tbl_event_data` WHERE deleted = 'N' AND 
			case 
			when '".$type."' = 'upcoming'
			then
			startDate >= DATE(NOW()) OR endDate >= DATE(NOW()) OR endDate IS NULL
			when '".$type."' = 'past'
			then
		    startDate < DATE(NOW()) OR endDate < DATE(NOW())
			else
			1=1
			end";

	   if($type == 'past')
      {	
		  $sql .= " ORDER BY startDate DESC";
	  }
	  else
	  {
       $sql .= " ORDER BY startDate DESC"; 
	  }		


		/*if ($offset > 0) {
			$sql .="".$offset.",";
		}*/
		//$sql .="".$limit."";
		$query = $this->db->query($sql);
		//echo $this->db->last_query(); exit;
		$res = $query->result_array();

		//echo "<pre>";		

		//print_r($res);exit();
		return $res;
    }

     public function eventInfo($data)
	{
		$str=base_url()."/uploads/eventImage/";
        // $query = $this->db->get_where('tbl_event_data',['deleted'=>'N','eventId'=>$data['eventId']]);
        // $result = $query->result_array();
         $sql = "Select eventId,eventName,eventVenue,eventDate,startDate,startTime,endDate,endTime,mobileNo,website,topic,otherInfo,concat('".$str."',eventImage) eventImage,createdBy,createdDate,updatedBy,updatedDate,deleted from tbl_event_data where deleted='N' and eventId='".$data['eventId']."' "; 
        $query = $this->db->query($sql);
        $result = $query->result_array();

        return $result;
	}
    
  public function getContents()
  {
  	$str=base_url()."/uploads/galleryData/";
 
	$sql="Select id,contentName,concat('".$str."',content) content,description,contentType,link,createdBy,createdOn,updatedBy,updatedOn,deleted from  tbl_gallery_content where deleted='N' order by createdOn desc";
		$query = $this->db->query($sql);
    $result = $query->result_array();
     return $result;
  }
	

public function getFileReport()
 {
   //echo "<pre>"; print_r($this->input->post());exit();

 	  $sql = "SELECT CONCAT(LEFT(report_unique_id,4),RIGHT(CONCAT('000',IFNULL(MAX(SUBSTR(report_unique_id,5)),0)+1),4)) AS uniqueId FROM tbl_file_reports WHERE LEFT(report_unique_id,4) = (SELECT CONCAT(IFNULL(t2.stateCode,'00'),IFNULL(t1.districtCode,'00')) FROM `tbl_district` AS t1 LEFT JOIN `tbl_state` AS t2 ON t1.stateId = t2.stateId WHERE t1.districtId = '".$this->input->post('districtIncidence')."') ";

		$query = $this->db->query($sql);	

		$result = $query->result_array();

		if(empty($result[0]['uniqueId']))
		{
			$sql1 = "SELECT CONCAT(IFNULL(t2.stateCode,'00'),IFNULL(t1.districtCode,'00'),'0001') AS uniqueId 
                      FROM `tbl_district` AS t1 LEFT JOIN `tbl_state` AS t2 ON t1.stateId = t2.stateId WHERE t1.districtId = '".$this->input->post('districtIncidence')."' ";

			$query1 = $this->db->query($sql1);	

		   $result1 = $query1->result_array();

		    $uniqueId = $result1[0]['uniqueId'];

		}else{

           $uniqueId = $result[0]['uniqueId'];
		}

   $data['report_unique_id'] = $uniqueId;		

 	$data['firstName'] = $this->input->post('fname');

 	$data['lastName'] = $this->input->post('lname');

 	$data['guardian'] = $this->input->post('guardian');

 	$data['age'] = $this->input->post('age');

 	$data['mobile'] = $this->input->post('mobile');

 	$data['address'] = $this->input->post('address');

 	$data['state'] = $this->input->post('state');

 	$data['district'] = $this->input->post('district');

	 if(!empty($this->input->post('incidenceDate')))	
	  {	

	 	$data['date_of_incidence'] = date('Y-m-d',strtotime($this->input->post('incidenceDate')));
	  }
 	$data['incidence_state'] = $this->input->post('stateIncidence');

 	$data['incidence_district'] = $this->input->post('districtIncidence');

 /*if(!empty($this->input->post('incidenceReport')))	

  {	
  	$data['date_of_incidence_reported'] = date('Y-m-d',strtotime($this->input->post('incidenceReport')));
  }*/

  $data['date_of_incidence_reported'] = date('Y-m-d');

 	$data['type_of_incidence'] = $this->input->post('incidenceType');

 	$data['by_whom'] = 	$this->input->post('byWhom');

 	$data['support_required'] = $this->input->post('support');

 	$data['description'] = $this->input->post('description');

 	$data['type_of_incidence_other'] = $this->input->post('incidenceTypeOther');

 	$data['by_whom_other'] = $this->input->post('byWhomOther');

 	$data['support_required_other'] = $this->input->post('supportOther');

 	//print_r($data);exit();

 	$this->db->insert('tbl_file_reports',$data);


 	$post['mobileNo'] = $data['mobile'];


 	$insert_id =  $this->db->insert_id();

 	$otp = mt_rand(100000,999999);

 	$this->db->where('id',$insert_id);

 	$this->db->update('tbl_file_reports',['otp'=>$otp]);

     $post['smsContent'] = 'Your Sahay Violence reporting otp is '.$otp;
     //new line


 	//$this->session->set_flashdata(['completeMessage'=>$post['smsContent']]);

 		$mobile = $post['mobileNo'];
		$smsContent = str_replace(' ','+',$post['smsContent']);
		//$smsTime = $data['smsTime'];
		//$date = date('d-m-Y').'T'.date('h:i:s');
		$smsApi = 'http://smsw.co.in/API/WebSMS/Http/v1.0a/index.php?username=codecube&password=cod123&sender=CODECB&to='.$mobile.'&message='.$smsContent.'&reqid=1&format={json|text}&route_id=route+id&callback=Any+Callback+URL&unique=0&sendondate=';
		//echo $smsApi; //exit;
		$ch = curl_init($smsApi);
		//curl_setopt($ch, CURLOPT_BUFFERSIZE, 8400000);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $smsContent);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mobile);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $smsTime);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain')); 
		$result2 = curl_exec($ch); // This is the result from the API
		//print_r($result2);exit;
		curl_close($ch);


 	return $insert_id;

 	
 }


 public function verifyFileReport($data)
 {

 	$this->db->select('id');

 	$this->db->from('tbl_file_reports');

 	$this->db->where('id',$data['reportId']);

 	$this->db->where('otp',$data['otp']);

 	$query = $this->db->get();

 	//echo $this->db->last_query();

 	$result = $query->result_array();

			//print_r($result);exit();

	return $result;	

 }


  public function resetFileReportOtp($data)
 {
   $otp = mt_rand(100000,999999);

 	$this->db->where('id',$data['reportId']);

 	$this->db->update('tbl_file_reports',['otp'=>$otp]);

 	$res  = $this->fileReportById($data);

     	  $post['smsContent'] = 'Your Sahay Violence reporting otp is '.$otp;


 		$mobile = $res[0]['mobile'];
		$smsContent = str_replace(' ','+',$post['smsContent']);
		//$smsTime = $data['smsTime'];
		//$date = date('d-m-Y').'T'.date('h:i:s');
		$smsApi = 'http://smsw.co.in/API/WebSMS/Http/v1.0a/index.php?username=codecube&password=cod123&sender=CODECB&to='.$mobile.'&message='.$smsContent.'&reqid=1&format={json|text}&route_id=route+id&callback=Any+Callback+URL&unique=0&sendondate=';
		//echo $smsApi; //exit;
		$ch = curl_init($smsApi);
		//curl_setopt($ch, CURLOPT_BUFFERSIZE, 8400000);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $smsContent);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mobile);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $smsTime);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain')); 
		$result2 = curl_exec($ch); // This is the result from the API
		//print_r($result2);exit;
		curl_close($ch);

 }

  public function fileReportById($data)
 {
 	/*$this->db->select('t1.incidence_addressed_internal,t1.incidence_addressed_external,t1.date_of_incidence_addressed,t1.type_of_services,t1.report_unique_id,t1.method_of_resolving,t1.status,t1.description,t1.reason,t1.createdDate');*/

 	$this->db->select('t1.*,t4.stateName as incidenceState,t5.districtName as incidenceDistrict');

 	$this->db->from('tbl_file_reports as t1');


 	$this->db->join('tbl_state as t4','t1.incidence_state = t4.stateId','left');

 	$this->db->join('tbl_district as t5','t1.incidence_district = t5.districtId','left');;

 	

 	$this->db->where('t1.deleted','N');

 	$this->db->where('t1.id',$data['reportId']);


 		$query = $this->db->get();

	 	$result = $query->result_array();

				//print_r($result);exit();

		return $result;	


 }


  public function fileReportVerified($data)
 {
   
 	$this->db->where('id',$data['reportId']);

 	$this->db->update('tbl_file_reports',['otpVerify'=>"Y"]);

 	$res  = $this->fileReportById($data);

 	$post['smsContent'] = 'You report with ID '.$res[0]['report_unique_id'].' has been registered with Sangraha. Please quote this number for future followup.';


 	$this->session->set_flashdata(['completeMessage'=>$post['smsContent']]);

 		$mobile = $res[0]['mobile'];
		$smsContent = str_replace(' ','+',$post['smsContent']);
		//$smsTime = $data['smsTime'];
		//$date = date('d-m-Y').'T'.date('h:i:s');
		$smsApi = 'http://smsw.co.in/API/WebSMS/Http/v1.0a/index.php?username=codecube&password=cod123&sender=CODECB&to='.$mobile.'&message='.$smsContent.'&reqid=1&format={json|text}&route_id=route+id&callback=Any+Callback+URL&unique=0&sendondate=';
		//echo $smsApi; //exit;
		$ch = curl_init($smsApi);
		//curl_setopt($ch, CURLOPT_BUFFERSIZE, 8400000);
		//curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $smsContent);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $mobile);
		//curl_setopt($ch, CURLOPT_POSTFIELDS, $smsTime);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		//curl_setopt($ch, CURLOPT_HTTPHEADER,     array('Content-Type: text/plain')); 
		$result2 = curl_exec($ch); // This is the result from the API
		//print_r($result2);exit;
		curl_close($ch);
		
 }



 
 public function fileReportHistory($reportId)
 {
 	$this->db->select('t1.incidence_addressed_internal,t1.incidence_addressed_external,t1.date_of_incidence_addressed,t1.type_of_services,t2.report_unique_id,t1.method_of_resolving,t1.status,t1.description,t1.reason,,t1.createdDate');

 	$this->db->from('report_audit as t1');

 	$this->db->join('tbl_file_reports as t2','t1.report_id = t2.id','left');

 	$this->db->where('t1.deleted','N');

 	$this->db->where('t2.report_unique_id',$reportId);
 	$this->db->limit(1);  
 	$query = $this->db->get();

 	$result = $query->result_array();

			//print_r($result);exit();

	return $result;	
 } 

 public function fileReportData($reportId)
 {
 	/*$this->db->select('t1.incidence_addressed_internal,t1.incidence_addressed_external,t1.date_of_incidence_addressed,t1.type_of_services,t1.report_unique_id,t1.method_of_resolving,t1.status,t1.description,t1.reason,t1.createdDate');*/

 	$this->db->select('t1.*,t4.stateName as incidenceState,t5.districtName as incidenceDistrict');

 	$this->db->from('tbl_file_reports as t1');


 	$this->db->join('tbl_state as t4','t1.incidence_state = t4.stateId','left');

 	$this->db->join('tbl_district as t5','t1.incidence_district = t5.districtId','left');;

 	

 	$this->db->where('t1.deleted','N');

 	$this->db->where('t1.report_unique_id',$reportId);


 		$query = $this->db->get();

	 	$result = $query->result_array();

				//print_r($result);exit();

		return $result;	


 }

 	public function serviceProviderDetail($serviceProviderId)
    {
        $sql="SELECT `name`,`address`,`mobile`,`email`,`skypeId`,`website`,`qualification`,`affiliation`,`day`,`time`,`conMode`,`conCharges`,`linkage`
			FROM tbl_service_provider_details WHERE deleted = 'N' AND serviceProviderId='".$serviceProviderId."'";	
		$query = $this->db->query($sql);
		$res = $query->result_array();		
		return $res;
    }

     public function getRegistrationMode($modeId)
    {
    	$sql = "SELECT `mode` FROM tbl_registration_modes WHERE modeid = '".$modeId."' ";

    	$query = $this->db->query($sql);

    	$result = $query->result_array();

    	return $result;
    }

     public function serviceProviderServices($serviceProviderId)
    {
     /* $sql = "SELECT `t1.serviceTypeParameterName` FROM tbl_service_type_parameters AS t1 LEFT JOIN tbl_service_provider_fields AS t2 ON t1.serviceTypeParameterId = t2.serviceTypeParameterId WHERE t2.serviceProviderId = '".$serviceProviderId."' ";	*/

      $sql = "SELECT t1.serviceTypeParameterName FROM tbl_service_type_parameters  AS t1  LEFT JOIN tbl_service_provider_fields AS t2 ON 
              t1.serviceTypeParameterId = t2.serviceTypeParameterId WHERE t2.serviceProviderId = '".$serviceProviderId."' AND t2.`value` IN ('Y','Yes') 
             GROUP BY t1.serviceTypeParameterId,t2.serviceTypeParameterId ";

      $query = $this->db->query($sql);

      $result = $query->result_array();

      return $result;

    }

    public function getComments($page_id)
	{
       $query = $this->db->get_where('tbl_pages_comments',['deleted'=>'N','comment_status'=>'approved','page_id'=>$page_id,'parent_id'=>0]);

       $result = $query->result_array();


        //return $result;

      if(!empty($result))
      {	
       foreach ($result as $value) 
		{
			$this->db->select('*');

			$this->db->from('tbl_pages_comments');

			$this->db->where(['deleted'=>'N','comment_status'=>'approved','page_id'=>$page_id]);

			$this->db->where('parent_id',$value['id']);

			$query1 = $this->db->get();

			$result1 = $query1->result_array();
		}

		$result3 = array_merge($result,$result1);

      }else{
      	$result3 = $result;
      }
  }


public function otpuserVerify(){
  		$this->db->set('deleted','N');
  		$this->db->set('userVerify','Y');
  		$this->db->where('userId',$this->input->post('userId'));
  		$this->db->where('otp',$this->input->post('otp'));
  		$this->db->update('tbl_user');
  		return $this->db->affected_rows();
    }

 public function checkUser($username)
	{
		$this->db->select('*');

		$this->db->from('tbl_user');

		$this->db->where('deleted','N');

		$this->db->where('userName',$username);

		$query = $this->db->get();

		$result = $query->result_array();

		return $result;
	}

	public function updateOtp($userId,$otp)
	{
			$array = ['deleted'=>'N','userId'=>$userId];
	        $this->db->where($array);

	        $this->db->update('tbl_user',['otp'=>$otp]);
	}

  public function insertOtp($userId,$otp)
  {
  	 $array = ['otp'=>$otp,'userId'=>$userId];

  	 $this->db->insert('tbl_otp_data',$array);
  }

   public function getUser($userId)
  {
     $query = $this->db->get_where('tbl_user',['deleted'=>'N','userId'=>$userId]);

     $result = $query->result_array();

     return $result;
  }

   public function setPassword($data)
  {
  	  $this->db->where(['deleted'=>'N','userId'=>$data['userId']]);

  	  $this->db->update('tbl_user',['password'=>$data['password']]);
  }

   public function setLogs($userId)
  {
  	  $array = ['userId'=>$userId,'logTime'=>date('Y-m-d h:i:s'),'logInto'=>'App'];

  	  $this->db->insert('tbl_login_logs',$array);
  }


  	 public function getLoginId($userId)
  {
        $this->db->select_max('id');

		$this->db->from('tbl_login_logs');

		$this->db->where('userId',$userId);

		$this->db->where('logInto','App');

		$query1 = $this->db->get();

        $result1 = $query1->result_array();

        return $result1;

  }

   public function testimonials(){
  	$name='testimonials-widget';
  	$sql='SELECT t1.`post_content`,t2.`meta_value`,(SELECT t4.meta_value FROM wp_posts AS t3 LEFT JOIN wp_postmeta AS t4 ON t3.ID=t4.post_id WHERE t3.`post_type`="testimonials-widget" AND t4.`meta_key`="testimonials-widget-location" AND t3.ID=t1.`ID`) AS location 
FROM wp_posts AS t1 LEFT JOIN wp_postmeta AS t2 ON t1.ID=t2.post_id WHERE t1.`post_type`="testimonials-widget" AND t2.`meta_key`="testimonials-widget-author"';
  	 $query = $this->db2->query($sql);
  	$result=$query->result_array();
  	return $result;
  }

  public function getFileReportFeedback()
 {
 		$this->db->select('id');

 	$this->db->from('tbl_file_reports');

 	$this->db->where('deleted','N');

 	$this->db->where('report_unique_id',$this->input->post('reportId'));

 		$query = $this->db->get();

	 	$result = $query->result_array();


		$this->db->select('id');

 	$this->db->from('tbl_file_reports_feedbacks');

 	$this->db->where('deleted','N');

 	$this->db->where('report_id',$result[0]['id']);

 		$query1 = $this->db->get();

	 	$result1 = $query1->result_array();
	 	//print_r( $result);
	 	//print_r($result1);

	if(!$result1[0]['id'])
	{

    $post['report_id' ] = $result[0]['id'];

    $post['part_one'] = $this->input->post('part1');

    $post['part_one_text'] = $this->input->post('parttext1');

    $post['part_two'] =  $this->input->post('part2');

    $post['part_two_text'] = $this->input->post('parttext2');

    $post['part_three'] = $this->input->post('part3');

    $post['part_three_text'] = $this->input->post('parttext3');

    $post['part_four'] = $this->input->post('part4');

    $post['part_four_text'] = $this->input->post('parttext4');

    $this->db->insert('tbl_file_reports_feedbacks',$post);

	} 	
 
   return $result1[0]['id'];
   
   exit();
 }

	public function stateName($stateId)
	{
	$sql="SELECT * FROM `tbl_state` WHERE deleted = 'N' AND stateId = '".$stateId."' ";	
	$query = $this->db->query($sql);
	$res = $query->result_array();		
	return $res;

	}

	public function districtName($districtId)
	{
	$sql="SELECT * FROM `tbl_district` WHERE deleted = 'N' AND districtId = '".$districtId."' ";	
	$query = $this->db->query($sql);
	$res = $query->result_array();		
	return $res;
	}

	public function getOnGroundPartner($data)
	{
	$sql = "SELECT * FROM `tbl_onground_partner_data` WHERE stateId = '".$data['stateId']."' OR districtId = '".$data['districtId']."' ";

	$query = $this->db->query($sql);

	$res = $query->result_array();

	return $res;

	}

	public function state() {				
	$sql="SELECT * FROM `tbl_state` WHERE deleted = 'N'";	
	$query = $this->db->query($sql);
	$res = $query->result_array();		
	return $res;
	}

	public function getVoucherNumber($quizNumber)
	{
	$sql = "SELECT voucherId,voucherNumber FROM `tbl_voucher_creation_data` WHERE uniqueQuizNumber = '".$quizNumber."' ";

	$query = $this->db->query($sql);

	$res = $query->result_array();

	return $res;
	}	

	public function getOnGroundPartnerById($id)
    {
       $query = $this->db->get_where('tbl_onground_partner_data',['deleted'=>'N','ongroundPartnerId'=>$id]);

       $result = $query->result_array();

       return $result;
    }


     public function getVoucherNumberById($id)
    {
    	$query = $this->db->get_where('tbl_voucher_creation_data',['voucherId'=>$id,'deleted'=>'N']);

    	$result = $query->result_array();

         return $result;
    }
	

	    public function getVoucherNumberById1($id)
    {
    	$query = $this->db->get_where('tbl_voucher_creation_data',['uniqueQuizNumber'=>$id,'deleted'=>'N']);

    	$result = $query->result_array();

         return $result;
    }

	public function redeemCoupon($data)
	{
	    $this->db->where('voucherId',$data['voucherId']);

	    $this->db->update('tbl_voucher_creation_data',['used'=>'No','ongroundPartnerId'=>$data['partnerId']]);

	    return TRUE;
	}


	public function userinfo($data)
	{
		$this->db->select('*');
		$this->db->from('tbl_user');
		$this->db->where('userId',$data);
		$query = $this->db->get();

		$result = $query->result_array();

		return $result;
	}	


}
