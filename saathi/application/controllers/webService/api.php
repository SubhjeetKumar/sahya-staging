<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
header('Access-Control-Allow-Origin: *'); 
header('Access-Control-Allow-Methods: POST');
header('Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept'); 

//define("GOOGLE_API_KEY", "AIzaSyA7tYOQH__KfBbY2DvuAejQCkg4e-M9_7Y");

class Api extends CI_Controller {
	
    public function __construct() {
        parent::__construct();
        $this->load->model('webService/apiModel', 'Api', TRUE);
       // $this->load->model('webService/apiModel', 'Api', TRUE);
       $this->load->model('role/rolemasterweb', 'roleMasterWeb', TRUE);
    }
    		
	public function login() {		
		if ($this->input->post()){
			$data['userName']=$this->input->post('userName');
			$data['password']=$this->input->post('password');
			$data['quizUniqueNumber']=$this->input->post('quizUniqueNumber');
			$data['loginTime']=$this->input->post('loginTime');
			$res=$this->Api->login($data);
			//echo '<pre>';print_r($res);exit;
			$response=$res[0];
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function searchServiceProvider() {		
		if ($this->input->post()){
			$data['searchText']=$this->input->post('searchText');
			$data['serviceTypeId']=$this->input->post('serviceTypeId');
			$data['serviceTypeParameterId']=$this->input->post('serviceTypeParameterId');
			$data['latLong']=$this->input->post('latLong');
			$res=$this->Api->searchServiceProvider($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='Service Provider Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function vouchersList() {		
		if ($this->input->post()){
			$data['userId']=$this->input->post('userId');
			$data['voucherId']=$this->input->post('voucherId');
			$res=$this->Api->vouchersList($data);
			//echo '<pre>';print_r($res);exit;
			if($this->input->post('voucherId')){
				$this->success=$res[0];
			}else{
				$this->success['responseMessage']='Vouchers Fetched Successfully';
				$this->success['responseCode']=200;
				$this->success['data']=$res;
			}
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function registerUser() {		
		if ($this->input->post()){
			$data['name']=$this->input->post('name');
			$data['age']=$this->input->post('age');
			if($this->input->post('dob')!=''){
			$data['dob']=date("Y-m-d",$this->input->post('dob'));}else{$data['dob']='';}
			$data['gender']=$this->input->post('gender');
			$data['email']=$this->input->post('email');
			$data['occupation']=$this->input->post('occupation');
			$data['educationLevel']=$this->input->post('educationLevel');
			$data['userName']=$this->input->post('userName');
			$data['password']=md5($this->input->post('password'));
			$data['district']=$this->input->post('district');	
			$data['state']=$this->input->post('state');	
			$data['placeofOrigin']=$this->input->post('placeofOrigin');	
			$data['mobile']=$this->input->post('mobile');	
			$data['maritalStatus']=$this->input->post('maritalStatus');	
			$data['behaviour']=$this->input->post('behaviour');	
			$data['hydc']=$this->input->post('hydc');	
			$res=$this->Api->registerUser($data);
			//echo '<pre>';print_r($res);exit;
			if($res[0]['responseCode'] == '200'){
				$resOtp=$this->Api->sendSms($res[0]);
			}

			$this->success['responseMessage']='Your registeration is half completed for login You need to verify by otp';
			$this->success['data']=$res[0];
			$this->success['responseCode']=200;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	
	public function forgotPassword() {		
		if ($this->input->post()){
			$data['userName']=$this->input->post('userName');
			$res=$this->Api->forgotPassword($data);
			//echo '<pre>';print_r($res);exit;
			$response=$res[0];
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function changePassword() {		
		if ($this->input->post()){
			$data['userId']=$this->input->post('userId');
			$data['oldPassword']=$this->input->post('oldPassword');
			$data['newPassword']=$this->input->post('newPassword');
			$res=$this->Api->changePassword($data);
			//echo '<pre>';print_r($res);exit;
			$response=$res[0];
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function eventsList() {		
		$data['eventId']=$this->input->post('eventId');
		$res=$this->Api->eventsList($data);
		//echo '<pre>';print_r($res);exit;
		if($this->input->post('eventId')){
			$this->success=$res[0];
		}else{
			$this->success['responseMessage']='Events Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
		}
		$response=$this->success;
		echo json_encode($response);
    }
	
	public function ongroundPartnersList() {		
		$data['ongroundPartnerId']=$this->input->post('ongroundPartnerId');
		$res=$this->Api->ongroundPartnersList($data);
		if($this->input->post('ongroundPartnerId')){
			$this->success=$res[0];
		}else{
			$this->success['responseMessage']='Onground Partner Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
		}
		$response=$this->success;
		echo json_encode($response);
    }
	
	public function otpVerification() {		
		if ($this->input->post()){
			$data['userId']=$this->input->post('userId');
			$data['otp']=$this->input->post('otp');
			$data['quizUniqueNumber']=$this->input->post('quizUniqueNumber');
			$res=$this->Api->otpVerification($data);
			//echo '<pre>';print_r($res);exit;
			$response=$res[0];
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	// public function serviceAccessVoucher() {		
	// 	if ($this->input->post()){
	// 		$data['userId']=$this->input->post('userId');
	// 		$data['serviceProviderId']=$this->input->post('serviceProviderId');
	// 		$res=$this->Api->serviceAccessVoucher($data);
	// 		//echo '<pre>';print_r($res);exit;
	// 		if($res[0]['responseCode'] == '200'){
	// 			$resOtp=$this->Api->sendSms($res[0]);
	// 		}
	// 		$response=$res[0];
	// 	}else{
	// 		$this->failed['responseMessage']='Field is Mandatory';
	// 		$this->failed['responseCode']=0;
	// 		$response=$this->failed;
	// 	}
	// 	echo json_encode($response);
 //    }
	
	public function playedQuizList() {		
		if ($this->input->post()){
			$data['userId']=$this->input->post('userId');
			$res=$this->Api->playedQuizList($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='Quiz List Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function quizList() {		
		$res=$this->Api->quizList();
		//echo '<pre>';print_r($res);exit;
		$this->success['responseMessage']='Quiz List Fetched Successfully';
		$this->success['responseCode']=200;
		$this->success['data']=$res;
		$response=$this->success;
		echo json_encode($response);
    }
	
	public function newQuizQuestions() {		
		if ($this->input->post()){
			$data['quizId']=$this->input->post('quizId');
			$res=$this->Api->newQuizQuestions($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='Quiz Questions Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function eventSearch() {		
		if ($this->input->post()){
			$data['name']=$this->input->post('name');
			$data['date']=$this->input->post('date');
			$res=$this->Api->eventSearch($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='Events Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function stateList() {		
		$res=$this->Api->stateList();
		//echo '<pre>';print_r($res);exit;
		$this->success['responseMessage']='States Fetched Successfully';
		$this->success['responseCode']=200;
		$this->success['data']=$res;
		$response=$this->success;
		echo json_encode($response);
    }
	
	public function districtList() {		
		if ($this->input->post()){
			$data['stateId']=$this->input->post('stateId');
			$res=$this->Api->districtList($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='Districts Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function addServiceProviderLocation() {		
		if ($this->input->post()){
			$data['userId']=$this->input->post('userId');
			$data['serviceProviderId']=$this->input->post('serviceProviderId');
			$data['lat']=$this->input->post('lat');
			$data['long']=$this->input->post('long');
			$res=$this->Api->addServiceProviderLocation($data);
			//echo '<pre>';print_r($res);exit;
			$response=$res[0];
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function menuList() {		
		if ($this->input->post()){
			$data['userType']=$this->input->post('userType');
			$res=$this->Api->menuList($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='Menu Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function serviceProviderType() {		
		$res=$this->Api->serviceProviderType();
		//echo '<pre>';print_r($res);exit;
		$this->success['responseMessage']='Service Provider Types Fetched Successfully';
		$this->success['responseCode']=200;
		$this->success['data']=$res;
		$response=$this->success;
		echo json_encode($response);
    }
	
	public function submitQuiz() {		
		if ($this->input->post()){
			$data['quizId']=$this->input->post('quizId');
			$data['quizStartTime']=$this->input->post('quizStartTime');
			$data['quizEndTime']=$this->input->post('quizEndTime');
			$data['quizQuestionId']=$this->input->post('quizQuestionId');
			$data['quizQuestionOptionId']=$this->input->post('quizQuestionOptionId');
			$data['userId']=$this->input->post('userId');
			$res=$this->Api->submitQuiz($data);
			$response=$res;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function feedback() {		
		if ($this->input->post()){
			$data['userId']=$this->input->post('userId');
			$data['serviceProviderId']=$this->input->post('serviceProviderId');
			$data['feedback']=$this->input->post('feedback');
			$res=$this->Api->feedback($data);
			$response=$res[0];
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function webView() {		
		if ($this->input->post()){
			$type=$this->input->post('type');
			$this->success['responseMessage']='Web View Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['url']= base_url().'index.php/webService/api/webViewUrl/'.$type;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function webViewUrl() {	
		$type = $this->uri->segment(4);
		$this->load->view('api/'.$type);
	}
	
	public function logout() {		
		if ($this->input->post()){
			$data['userId']=$this->input->post('userId');
			$data['logoutTime']=$this->input->post('logoutTime');
			$res=$this->Api->logout($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='User Logout Successfully';
			$this->success['responseCode']=200;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }
	
	public function serivceTypeParameters() {		
		if ($this->input->post()){
			$data['serviceTypeId']=$this->input->post('serviceTypeId');
			$res=$this->Api->serivceTypeParameters($data);
			//echo '<pre>';print_r($res);exit;
			$this->success['responseMessage']='Service Type Parameters Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$res;
			$response=$this->success;
		}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}
		echo json_encode($response);
    }

   public function userRegistrationConsent()
   {
   	  $data['mobile'] = $_GET['your_sender'];

   	  $data['smsContent'] = $_GET['your_message'];

   	 /* $data['mobile'] = $this->input->post('mobile');

        $data['smsContent'] = $this->input->post('smsContent');*/
    
   	   $result = $this->Api->userRegistrationConsent($data);

   	  $this->success['responseMessage']='User registration consent SMS Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$result;
			$response=$this->success;
	/*	}else{
			$this->failed['responseMessage']='Field is Mandatory';
			$this->failed['responseCode']=0;
			$response=$this->failed;
		}*/
		echo json_encode($response);

   }

   public function campReportUniqueId()
   {
   	 $query = $this->db->get('tbl_camp_reports');

   	 $result = $query->result_array();

   	 foreach ($result as $key => $value) 
   	 {
   	 	
   	 	$campReportUniqueId = $this->Api->campReportUniqueId();

   	 	$this->db->where('id',$value['id']);

   	 	$this->db->update('tbl_camp_reports',['camp_code_unique_id'=>$campReportUniqueId]);
   	 }
   }




	public function searchServiceProviders()
	{

	if($this->input->post())
	{
	        $post['stateId'] = $this->input->post('stateId');
	        $post['districtId']=$this->input->post('districtId');
	        $post['serviceTypeId']=$this->input->post('serviceTypeId');
	        $post['serviceTypeParameterId']=$this->input->post('serviceTypeParameterId');
	        $post['latLong']=$this->input->post('latLong');
	        $result=$this->Api->searchServiceProviders($post);


	        	$this->success['responseMessage']='Service Fetched Successfully';
				$this->success['responseCode']=200;
				$this->success['data']=$result;
				$response=$this->success;


	    }
	    else{
	    		$this->failed['responseMessage']='Field is Mandatory';
				$this->failed['responseCode']=0;
				$response=$this->failed;

	    }

	    	echo json_encode($response);
	}

	public function event()
	{
		if($this->uri->segment(4))
		{
			//$data['type'] = $this->uri->segment(3);
			$data['type'] = $this->uri->segment(4);
			$data['eventList']=$this->Api->eventList($data['type']);
				$this->success['responseMessage']='Event Fetched Successfully';
				$this->success['responseCode']=200;
				$this->success['data']=$data;
				$response=$this->success;
			
		}else
		{
				$this->failed['responseMessage']='Please specify Event name/type';
				$this->failed['responseCode']=0;
				$response=$this->failed;
		}
        echo json_encode($response);      
    }

    public function eventInfo()
    {
    	$data['eventId'] = $this->input->post('eventId');

    	$main = $this->Api->eventInfo($data);

    	echo json_encode($main);
    }
    
     public function gallery()
    {

    	$data['contentList'] = $this->Api->getContents();
    	$this->success['responseMessage']='Gallery Fetched Successfully';
		$this->success['responseCode']=200;
		$this->success['data']=$data;
		$response=$this->success;
		echo json_encode($response);

    }


    public function quiz()
	{ 

		$data['quizList']=$this->Api->quizList();
		$this->success['responseMessage']='QuizList Fetched Successfully';
		$this->success['responseCode']=200;
		$this->success['data']=$data;
		$response=$this->success;
		echo json_encode($response);
		   
    }

     public function quizQuestions()
    { 
    	//if($this->uri->segment(4))
    	if($this->input->post('quizId'))
    	{
			//$data['quizId'] = $this->uri->segment(4);
			$data['quizId'] = $this->input->post('quizId');
			$data['quizName']     = $this->roleMasterWeb->quizName($data['quizId']);
			$data['quizQuestionsList']=$this->Api->newQuizQuestions($data);
			//echo '<pre>';print_r($data['quizQuestionsList']);exit;
			$this->success['responseMessage']='Quiz Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$data;
			$response=$this->success;
		}else{
				$this->failed['responseMessage']='Please specify QuizName';
				$this->failed['responseCode']=0;
				$response=$this->failed;
		}
		echo json_encode($response);
		       
    }

    public function showMessage()
	{
		$data['optionId'] =$this->input->post('optionId');
		$main = $this->roleMasterWeb->showMessage($data['optionId']);
		$res = json_encode($main);
		echo $res; 
	}

	public function playedQuiz()
	{ 
		$data['userId']=$this->session->userdata('userId');
		$data['playedQuizList']=$this->Api->playedQuizList($data);
		if(!empty($data['playedQuizList']))
		{
			$this->success['responseMessage']='Quiz Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$data;
			$response=$this->success;
		}else{
				$this->failed['responseMessage']='There is no playedQuiz';
				$this->failed['responseCode']=0;
				$response=$this->failed;
		}
		echo json_encode($response);
    }

    // public function otpVerify()
    // {
    	
    // 	if ($this->input->post()) {
    		
    // 		$res = $this->roleMasterWeb->otpVerify($data);

    // 		if ($res['0']['otp'] == $this->input->post('otp')) {
    			

    // 			$this->roleMasterWeb->updateVerify();
    		
				// $data['userName'] 		= $res['0']['userName'];
				// $data['name'] 			= $res['0']['name'];
				// $data['emailAddress']	= $res['0']['emailAddress'];
				// $data['password'] 		= $res['0']['password'];
				// $data['mobileNo'] 		= $res['0']['mobileNo'];
				// $this->success['responseMessage']='User Verified Successfully';
				// $this->success['responseCode']=200;
				// $this->success['data']=$data;
				// $response=$this->success;
		 		
		  //      // $this->load->view('Layout/dashboardLayoutWeb',$data);
    // 		}else{
    // 			//$this->session->set_flashdata(['errorMessage'=>'please insert correct otp']);
    // 			//redirect(base_url()."homeweb/otp/".$this->uri->segment(3));
    // 			$this->failed['responseMessage']='please insert correct otp';
				// $this->failed['responseCode']=0;
				// $response=$this->failed;
    // 		}
    // 		echo json_encode($response);
    // 	}else{
    // 		$this->failed['responseMessage']='please Enter otp';
				// $this->failed['responseCode']=0;
				// $response=$this->failed;
    // 	}

    // }

   public function getFileReport()
	{
		if($this->Api->getFileReport()!=''){
	    	$data['reportId'] = $this->Api->getFileReport();
	    	$this->success['responseMessage']=' Fetched Successfully';
			$this->success['responseCode']=200;
			$this->success['data']=$data;
			$response=$this->success;
		}
		else{
				$this->failed['responseMessage']='Report is not submitted';
				$this->failed['responseCode'] =0;
				$response=$this->failed;

			}
			echo json_encode($response);
		
	    	
	}

	public function verifyFileReport()
	{

		$data['otp'] = $this->input->post('otp');
		$data['reportId'] = $this->input->post('reportId');
		$result =   $this->Api->verifyFileReport($data);

    	//print_r($result);exit();

    	if(!$result)
    	{
    		$data['msg'] = 'Wrong OTP'; 

 			// $this->session->set_flashdata(['otpMessage'=>'Wrong OTP']);

    		$this->Api->resetFileReportOtp($data);
    		$this->failed['responseMessage'] = 'Wrong Otp is Entered';
    		$this->failed['responseCode'] = 0;
    		$res=$this->failed;
    		// $data['content'] = 'web/verifyFileReport';


    	// $this->load->view('Layout/dashboardLayoutWeb',$data);

    	}else{

    		//echo "hjkhjk";
    		$this->Api->fileReportVerified($data);
    		
    		
    		$this->success['responsecode'] = 200;
    		$this->success['responseMessage'] = 'Report is submitted successfully and verified';
    		$res=$this->success;
    		// redirect(base_url().'homeweb/fileReport');

    	}	

    		echo json_encode($res);

    }

    public function showFileReport()
    {
    	if(!empty($this->input->post()))
    	{
       	  $data['reportId'] = $this->input->post('reportId');

       	  $data['reportHistory'] = $this->Api->fileReportHistory($data['reportId']);

       	  $data['reportData'] = $this->Api->fileReportData($data['reportId']);

       	  $this->success['responsecode']=200;
       	  $this->success['responseMessage']= 'Report fetched';
       	  $this->success['data']=$data;
       	  $res=$this->success;
       	}
       	else{
       			$this->failed['responsecode']=0;
       			$this->failed['responseMessage']='Please provide reportId';
       			$res=$this->failed;
       	}
       		
       		echo json_encode($res);
           // print_r($data['reportData']);exit();
             //		$data['content'] = 'web/showFileReport';

        	//$this->load->view('Layout/dashboardLayoutWeb',$data);     		
       

    }

    public function serviceProviderDetails()
    {
    	 $serviceProviderId = $this->input->post('serviceProviderId');

         $data['id'] = $serviceProviderId;

    	 $data['stateId'] = $this->input->post('stateId');

    	 $data['districtId'] = $this->input->post('districtId');

    	 $data['serviceTypeId'] = $this->input->post('serviceTypeId');

    	 $data['serviceTypeParameterId'] = $this->input->post('serviceTypeParameterId');

    	 $data['latlong'] = $this->input->post('latlong');

    	$data['serviceProviderId'] = $serviceProviderId;

    	$data['serviceProviderDetails'] = $this->Api->serviceProviderDetail($serviceProviderId);

    	$data['conMode'] = $this->Api->getRegistrationMode($data['serviceProviderDetails'][0]['conMode']);

    	$data['services'] = $this->Api->serviceProviderServices($serviceProviderId);

    	if($data['serviceProviderDetails']!=''){
    		$this->success['responseCode']=200;
    		$this->success['responseMessage']='OK';
    		$this->success['data']=array_merge($data['serviceProviderDetails'],$data['services']);
    		$res=$this->success;
    	}else{
    			$this->failed['responsecode']=0;
    			$this->failed['responseMessage']='Sorry Details are not available';
    			$res=$this->failed;
    	}
    	echo json_encode($res);

    }

    public function serviceAccessVoucher()
	{
		$serviceProviderId = $this->input->post('serviceProviderId');
		$data['userId']=$this->input->post('userId');
		$data['serviceProviderId']=$serviceProviderId;
		$data['voucherCode'] = 'VC'.mt_rand(10000,99999);
		if($data['serviceProviderId']){
			$data['accessVoucherDetail']=$this->Api->serviceAccessVoucher($data);

			$data1['mobileNo'] = $this->input->post('mobile');
			$data1['smsContent'] = "Hello ".$this->input->post('userName')." your voucher number for ".$data['accessVoucherDetail'][0]['name']." is ".$data['accessVoucherDetail'][0]['voucherNumber']." and your voucher details have been sent to Service Provider also";

			$this->Api->sendSms($data1);

			$data2['mobileNo'] = $data['accessVoucherDetail'][0]['mobile'];

			$data2['smsContent'] = "The voucher no. for ".$this->input->post('userName')." is ".$data['accessVoucherDetail'][0]['voucherNumber'].". The user would be contacting you for gifts".

			$this->Api->sendSms($data2);

		    $data3['mobileNo'] = $this->input->post('mobile');
			$data3['smsContent'] = "Hello,".$this->input->post('userName')." your voucher code generated is ".$data['voucherCode']." share this voucher code once you visits selected service provider ".$data['accessVoucherDetail'][0]['name'];

			$this->Api->sendSms($data3);
			
		}
	}




    

    public function otpuserVerify(){
		if($this->input->post())
		{
			$data=$this->Api->otpuserVerify($this->input->post());
			if($data!='' || $data!=null){
				$this->success['responseCode']=200;
				$this->success['responseMessage']="User is verified Successfully";
				$this->success['userdata']=$this->Api->userinfo($this->input->post('userId'));
				$res=$this->success;
			}
			else{
					$this->failed['responsecode']=0;
       				$this->failed['responseMessage']='sorry some issues occur';
       				$res=$this->failed;		
			}
		}else{
			$this->failed['responsecode']=0;
       			$this->failed['responseMessage']='Please provide correct userId and otp';
       			$res=$this->failed;
       	}
       		
       		echo json_encode($res);
	}

	public function checkUser()
    {
        $username = $this->input->post('username');

       $result = $this->Api->checkUser($username);

       if(!empty($result))
       {
          $data['mobile'] = $result[0]['mobileNo'];

       	  $data['userId'] = $result[0]['userId'];

       	//  $data['content'] = 'web/verifyUser';

       	  $otp = mt_rand(10000,99999);

       	  $this->Api->updateOtp($result[0]['userId'],$otp);

       	  $this->Api->insertOtp($result[0]['userId'],$otp);

       	  $message = "Your OTP to reset password in Sahay App is ".$otp;

       	  $message = str_replace(' ','%20',$message);

       	  $smsTime = date('d-m-Y');

       	 $smsApi = 'http://smsw.co.in/API/WebSMS/Http/v1.0a/index.php?username=codecube&password=cod123&sender=CODECB&to='.$data['mobile'].'&message='.$message.'&reqid=1&format={json|text}&route_id=route+id&callback=Any+Callback+URL&unique=0&sendondate='.$smsTime;
	
		        $ch = curl_init();
	           curl_setopt($ch, CURLOPT_URL,$smsApi);
	           curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	           curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	             curl_exec($ch);  

	             curl_close($ch);


       	  $this->success['responseCode']=200;
       	  $this->success['responseMessage']='Otp is sent to mobile';
       	  $this->success['data']=$data;
       	  $res=$this->success;
       }
       else{
             $this->failed['responseCode']='0';
             $this->failed['responseMessage']='User with this user name does not exist';
    		 $res=$this->failed;
       }
        echo json_encode($res);
    }

     public function verifyUser()
    {

      $userId = $this->input->post('userId');

      $mobile = $this->input->post('mobile');

      $result = $this->Api->getUser($userId);

      if($result[0]['otp'] == $this->input->post('otp'))
      {
      	
      	$this->success['responseCode']=200;
      	$this->success['responseMessage']='otp is verified';
      	//enter password for reset
      	$res=$this->success;
        
      }
      else{

            $data['userId'] = $userId;

            $data['mobile'] = $mobile;

            $otp = mt_rand(10000,99999);

       	  $this->Api->updateOtp($userId,$otp);

       	 $this->Api->insertOtp($userId,$otp);

       	  $message = "Your OTP to reset password in Sahay App is ".$otp;

       	  $message = str_replace(' ','%20',$message);

       	  $smsTime = date('d-m-Y');

       	 $smsApi = 'http://smsw.co.in/API/WebSMS/Http/v1.0a/index.php?username=codecube&password=cod123&sender=CODECB&to='.$mobile.'&message='.$message.'&reqid=1&format={json|text}&route_id=route+id&callback=Any+Callback+URL&unique=0&sendondate='.$smsTime;
	
		        $ch = curl_init();
	           curl_setopt($ch, CURLOPT_URL,$smsApi);
	           curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	           curl_setopt($ch, CURLOPT_TIMEOUT, 30);
		       curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	             curl_exec($ch);  

	             curl_close($ch);

	         $this->failed['responseCode'] = 0;
            $this->failed['responseMessage'] = 'Incorrect OTP please enter new otp sent to your mobile';

           // $data['content'] = 'web/verifyUser';

      	 	$res=$this->failed;
    			  	
      }

      echo json_encode($res);

    }


        public function setPassword()
    {
    	if($this->input->post('password')!=''&& $this->input->post('userId'))
    	{
    		$data['userId'] = $this->input->post('userId');
    	 	
    	 	    	$data['password'] = md5($this->input->post('password'));
    	 	
    	 	    	$this->Api->setPassword($data);
    	 	
    	 	    	$result = $this->Api->getUser($data['userId']);
    	 	
    	 	    	$setlogs = $this->Api->setLogs($data['userId']);
    	 	
    	 	    	//$result1 = $this->Api->getLoginId($data['userId']);
    	 	 // echo "<pre>";
    	 	 // echo var_dump($result);exit();
    	 	    	$this->success['responseCode']=200;
    	 	    	$this->success['responseMessage']='Password is changed';
    	 	    	$this->success['data'] =$result;
    	 	    	$res=$this->success;
    	 	   }
    	 	   else{

    	 	   		$this->failed['responseCode'] = 0;
    	 	   		$this->failed['responseMessage'] = 'UserId and password is empty';
    	 	   		$res=$this->failed;
    	 	   }

    	 	  	echo json_encode($res);

    	
    }

    public function testimonials()
    {
    	$data['result']=$this->Api->testimonials();
    	$this->success['responseCode']=200;
    	$this->success['responseMessage']='Testimonials Data';
    	$this->success['data'] =$data['result'];
    	$res=$this->success;
    	echo json_encode($res);
    }

 	public function getFileReportFeedback()
    {
      $main = $this->Api->getFileReportFeedback();

      if($main)
      {
      	  //$this->session->set_flashdata(['feedbackSubmitMessage'=>'Feedback already submitted']);
      		$this->failed['responseCode']=0;
    		$this->failed['responseMessage']='Feedback already submitted';
    		$res=$this->failed;
      }	else{
      			$this->success['responseCode']=200;
	    	$this->success['responseMessage']='Thank you for the review';
	    	//$this->success['data'] = $main;
    		$res=$this->success;
      }
      echo json_encode($res);
      
    }


    
    public function getGiftCoupon()
	{
		if($this->input->post())
		{
            $data['stateId'] = $this->input->post('state');
            $data['districtId'] = $this->input->post('districtId');
            $data['state'] = $this->Api->stateName($data['stateId']);
            $data['district'] = $this->Api->districtName($data['districtId']);
            $data['ongroundPartners'] = $this->Api->getOnGroundPartner($data);
           if(empty($data['ongroundPartners']))
           {

           	  $this->failed['responseCode']=0;
           	  $this->failed['responseMessage'] = 'Sorry,There are no Onground partners in '.$data['state'][0]['stateName'];
               if(!empty($data['district']))
               {
               	 $this->failed['responseMessage'] .= 'in '.$data['district'][0]['districtName'];
               }

               $this->failed['responseMessage'] .= " .Please select some other State/District.";
               $res=$this->failed;
              // echo json_encode($res);
           }
           else
           {
				$this->success['responseCode']=200;
		    	$this->success['responseMessage']= 'Gift Coupon is fetched Successfully';
		    	$this->success['result']=$data['ongroundPartners'];
		    	$res= $this->success;
           }

           echo json_encode($res);
              
           
		}	
		
	}

	public function getGiftCouponNo()
	{
	if($this->input->post('quizNumber'))
	{
		$quizNumber = $this->input->post('quizNumber');

		$data['quizNumber'] = $quizNumber;

		$data['accessVoucherDetail'] = $this->Api->getVoucherNumber($quizNumber);

		$this->success['responseCode']=200;
		$this->success['responseMessage']= $data['accessVoucherDetail'];
		$res= $this->success;	
	}
	else
	{
		$this->failed['responseCode']=0;
		$this->failed['responseMessage']='QuizNumber is mandatory';
		$res=$this->failed;
	}

	echo json_encode($res);
	}

 	public function redeemCoupon()
    {
       $data['voucherId'] = $this->input->post('voucherId');

       $data['partnerId'] = $this->input->post('partnerId');

       $result = $this->Api->getOnGroundPartnerById($data['partnerId']);

       $result2 =  $this->Api->getVoucherNumberById($data['voucherId']);

      // print_r($result2);exit();

      $result1 = $this->Api->redeemCoupon($data);


       if(!empty($result) && !empty($result1))
       {
       	 $data1['mobileNo'] = $this->input->post('mobile');

       	 $data1['smsContent'] = "Congrats ".$this->session->userdata('userName')." ! Your Gift Coupon is ".$result2[0]['voucherNumber']." Please call ".$result[0]['name']." on ".$result[0]['mobile'].". Your Code is ".$result2[0]['voucherCode'];

                  $this->Api->sendSms($data1);

            $data2['mobileNo']  = $result[0]['mobile'];
           $data2['smsContent'] = "Coupon: ".$result2[0]['voucherNumber']." ;Date: ".date('d M Y',strtotime($result2[0]['voucherDate']))." ;Mobile: ".$this->session->userdata('mobile')." ;Partner Id: ".$result[0]['ongroundPartnerUniqueId'];

            $this->Api->sendSms($data2); 

            $this->success['responseCode']=200;
            $this->success['responseMessage']= 'Your voucher is sent to your mobile';
            $this->success['data']= $result2;	
            $res=$this->success;
       }else
       {

		    $this->failed['responseCode']=0;
    		$this->failed['responseMessage']='This coupon is already redeemed';
    		$res=$this->failed;	
       }

       echo json_encode($res);
    }




}



