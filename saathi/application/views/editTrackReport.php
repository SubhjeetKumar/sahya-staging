<style>

 .input-group1 {
    position: relative;
    display: table;
    border-collapse: separate;
}
.none{
	display:none !important;
}
</style>

 <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2>Update Violence Report</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="">Home</a>
                        </li>
                        <li class="active">
                            <strong>Update Violence Report</strong>
                        </li>
                    </ol>
                </div>
                <div class="col-lg-2">

                </div>
            </div>
        <div class="wrapper wrapper-content animated fadeInRight">
			<?php if($this->session->flashdata('message')){ ?>
			<div class="alert alert-success fade in">
				<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
				<i class="fa fa-check-circle fa-fw fa-lg"></i>
				<?php echo $this->session->flashdata('message'); ?>.
			</div>
			<?php } ?>
           
            <div class="row">
				<div class="col-lg-12">
							<ul class="nav nav-tabs" style="background-color:white;">
                                 <li class="active"><a data-toggle="tab" href="#tab-2"><i class="fa fa-user"></i> Update Violence Report </a></li>
								        <?php// echo "<pre>";//print_r($reportData);?>
                                
							</ul>
                    <div class="ibox float-e-margins">
					<div class="tab-content">
					 <div id="tab-2" class="tab-pane active">
						   <div class="ibox-title">
                            <h5>Update Violence Report</h5>
                            <div class="ibox-tools">
                                <a class="collapse-link">
                                    <i class="fa fa-chevron-up"></i>
                                </a>
                                <a class="close-link">
                                    <i class="fa fa-times"></i>
                                </a>
                            </div>
                        </div>
                        <div class="ibox-content">
                            <form method="post" class="form-horizontal" enctype="multipart/form-data" action="<?php echo base_url(); ?>index.php/home/updateTrackReport">
                              <?php// print_r($reportData[0]['report_unique_id']);  ?>
                                <input type="hidden" name="reportId" value="<?php echo $reportId ?>">
                                <div class="form-group">
                                  <div class="col-sm-6">
                                    <label class="control-label">Incident ID</label>
                                    <input type="text" class="form-control" readonly="" value="<?php echo($reportData[0]['report_unique_id']);  ?>">
                                  </div>
                                  <div class="col-sm-6">
                                    <label class="control-label">Support Required</label>
                                      <input type="text" class="form-control" readonly="" value="<?php echo($reportData[0]['support_required']);  ?>">
                                  </div>
                                </div>
                                <div class="form-group">
                                  <div class="col-sm-6">
                                    <label class="control-label">First Name</label>
                                      <input type="text" class="form-control" readonly="" value="<?php echo($reportData[0]['firstName']);  ?>">
                                  </div>
                                   <div class="col-sm-6">
                                    <label class="control-label">last Name</label>
                                      <input type="text" class="form-control" readonly="" value="<?php echo($reportData[0]['lastName']);  ?>">
                                  </div>
                                </div>

                                   <div class="form-group">
                                  <div class="col-sm-6">
                                     <label class="control-label">Date of Incident</label>
                                     <div class="input-group1 date"> 
                                           <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>  
                                        <input autocomplete="off" type="text" disabled value="<?php if($reportData[0]['date_of_incidence'])
                                       echo  date('d/m/Y',strtotime($reportData[0]['date_of_incidence']))  ;  ?>" class="form-control">
                                        </div>
                                  </div>

                                  <div class="col-sm-6">
                                     <label class="control-label">Date of Incident Reporting </label>
                                     <div class="input-group1 date"> 
                                           <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>  
                                        <input autocomplete="off" type="text" disabled value="<?php if($reportData[0]['date_of_incidence_reported'])
                                       echo  date('d/m/Y',strtotime($reportData[0]['date_of_incidence_reported']))  ;  ?>" class="form-control">
                                        </div>
                                  </div>
                                  
                                </div>


                              <div class="hr-line-dashed"></div>    
                          	<div class="form-group">
                        			<div class="col-sm-6">
                                        <label class="control-label">Incident addressed by whom(Internal)</label> 
                                      
                        			<!-- 	<input type="text" name="incidenceAddressInternal" class="form-control"> -->

                                    <select class="form-control" name="incidenceAddressInternal">
                                        <option value="" <?php if($reportData[0]['incidence_addressed_internal']==''){ echo 'selected';} ?>>-Select-</option>
                                      <!--   <option> One to choose from Crisis Support Peer</option> -->
                                      <option value="Crisis Support Peer" <?php if($reportData[0]['incidence_addressed_internal']=='Crisis Support Peer'){ echo 'selected';} ?>>Crisis Support Peer</option>
                                        <option value="Training and Advocacy Coordinator" <?php if($reportData[0]['incidence_addressed_internal']=='Training and Advocacy Coordinator'){ echo 'selected';} ?>> Training and Advocacy Coordinator</option>
                                        <option value="State Program Manager"  <?php if($reportData[0]['incidence_addressed_internal']=='State Program Manager'){ echo 'selected';} ?>> State Program Manager</option>
                                    </select>
                                  
                        			</div>
                                    <div class="col-sm-6">
                                      <label class="control-label">Incident addressed by whom(External)</label> 
                                        
                                        <!-- <input type="text" name="incidenceAddressExternal" class="form-control"> -->

                                        <select onchange="checkAddressExternal()" class="form-control" id="incidenceAddressExternal" name="incidenceAddressExternal">
                                                    <option value="" <?php if($reportData[0]['incidence_addressed_external']==''){echo 'selected';} ?>>-Select-</option>
                                      <!-- <option>Another to chosse from Law </option> -->
                                     <option value="Law" <?php if($reportData[0]['incidence_addressed_external']=='Law'){echo 'selected';} ?> > Law </option>
                                    <option value="Police" <?php if($reportData[0]['incidence_addressed_external']=='Police'){echo 'selected';} ?> > Police</option>
                                    <option value="CBOs" <?php if($reportData[0]['incidence_addressed_external']=='CBOs'){echo 'selected';} ?> > CBOs</option>
                                   <option value="Boards" <?php if($reportData[0]['incidence_addressed_external']=='Boards'){echo 'selected';} ?> > Boards</option>
                                    <option value="Commission" <?php if($reportData[0]['incidence_addressed_external']=='Commission'){echo 'selected';} ?> > Commission</option>
                                      <option value="Others" <?php if($reportData[0]['incidence_addressed_external']=='Others'){echo 'selected';} ?> >Others</option>
                                   </select> 

                                    </div>
                                    </div>

                                    <div class="form-group">
                                	  <div class="col-sm-6" id="addressExternalOtherDiv" style="display: none;">
                                  	<label class="control-label">Incident addressed by whom(External) Other</label>
                                  	 <input type="text" class="form-control" id="addressExternalOther" name="addressExternalOther">
                                  </div>
                                    </div>

                                <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="control-label"> Date of incident addressed</label> 
                                        <div class="input-group date"> 
                                           <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>  
                                        <input id="dofia" autocomplete="off" type="text" name="dateIncidenceAddress" value="<?php if($reportData[0]['date_of_incidence_addressed'])
                                       echo  date('d/m/Y',strtotime($reportData[0]['date_of_incidence_addressed']))  ;  ?>" class="form-control">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                      <label class="control-label">Types of services provided</label> 
                                       
                                       <!--  <input type="text" name="serviceType" class="form-control"> -->
                                   <select name="serviceType[]" id="serviceType" onchange="checkServiceType()" class="chosen-select" multiple="">
                                       <option <?php if($reportData[0]['type_of_services']=='Counseling'){echo 'selected';} ?>>Counseling </option>
                                       <option <?php if($reportData[0]['type_of_services']=='Legal Support'){echo 'selected';} ?>>Legal Support </option>
                                       <option <?php if($reportData[0]['type_of_services']=='Police Intervention'){echo 'selected';} ?>>Police Intervention </option>
                                       <option <?php if($reportData[0]['type_of_services']=='Community Support'){echo 'selected';} ?>>Community Support </option>
                                       <option <?php if($reportData[0]['type_of_services']=='Others'){echo 'selected';} ?>>Others</option>
                                  </select>
                                    </div>
                                    </div>

                                    <div class="form-group">
                                      <div class="col-sm-6"  id="serviceTypeOtherDiv" style="display: none;" >
                                        <label class="control-label">Types of services provided - Others</label>
                                        <input type="text" class="form-control" id="serviceTypeOther" name="serviceTypeOther">
                                      </div>
                                    
                                    </div>    

                                   <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="control-label">Method of resolving (Formal / Informal)  </label> 
                                      <!-- 
                                        <input type="text" name="methodResolve" class="form-control"> -->
                                        <select name="methodResolve" class="form-control">
                                           <option value="" <?php if($reportData[0]['method_of_resolving']==''){echo 'selected';}?> >-select-</option> 
                                           <option value="Formal"  <?php if($reportData[0]['method_of_resolving']=='Formal'){echo 'selected';}?> >Formal</option>
                                           <option value="Informal" <?php if($reportData[0]['method_of_resolving']=='Informal'){echo 'selected';}?> >Informal</option>
                                        </select>
                                  
                                    </div>
                                    <div class="col-sm-6">
                                      <label class="control-label">Status</label> 
                                     
                                      <select class="form-control" name="status">
                                          <option value="" <?php if($reportData[0]['status']==''){echo 'selected';}?> >-Select Status-</option>
                                          <option value="OPEN" <?php if($reportData[0]['status']=='OPEN'){echo 'selected';}?> >Open</option>
                                          <option value="SOLVED" <?php if($reportData[0]['status']=='SOLVED'){echo 'selected';}?> >Solved</option>
                                          <option value="IN-PROGRESS" <?php if($reportData[0]['status']=='IN-PROGRESS'){echo 'selected';}?> >In-progress</option>
                                          <option value="REVOKED" <?php if($reportData[0]['status']=='REVOKED'){echo 'selected';}?> >Revoked</option>
                                      </select>
                                    
                                    </div>
                                    </div>        		


                                   <div class="form-group">
                                    <div class="col-sm-6">
                                        <label class="control-label">Brief description  </label> 
                                       
                                       <!--  <input type="text" name="description" value="<?php //echo $reportData[0]['description']; ?>" class="form-control"> -->
                                       <textarea name="description" class="form-control"><?php echo $reportData[0]['description']; ?></textarea>
                                   
                                    </div>
                                    <div class="col-sm-6">
                                      <label class="control-label">If pending. reason's ?</label> 
                                        
                                        <input type="text" name="reason" value="<?php echo $reportData[0]['reason']; ?>" class="form-control">
                                    
                                    </div>
                                    </div>                		
							 
                    								<div class="hr-line-dashed"></div>
                    								<div class="form-group">
                                    <div class="col-sm-4 col-sm-offset-2">
                                        <a href="<?php echo base_url(); ?>index.php/home/trackReport" class="btn btn-white">Cancel</a>
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>

                        
                        </div>
						 </div>
						 
						
						
						</div>
                    </div>
                </div>
				
				
				
				
				
            </div>
        </div>
        <div class="footer">
            
        </div>

        </div>
        </div>
		
<script type="text/javascript">
   
   window.onload=function(){
            $('#dofia').datepicker({changeYear: true, changeMonth: true,format: 'dd/mm/yyyy', endDate: new Date()});
          };
      
        function getDistrict(){
            var state = $('#state').val();
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>index.php/home/getDistrict",
                data: {state:state},
                success: function(data) {
                    
                    var rslt = $.trim(data);
                    result = JSON.parse(rslt);
                    var len = result.length;

                    htm = '<option value="" readonly>Select District</option>';
                    for(var i = 0; i < len; i++){
                        
                            htm += '<option value="'+result[i].districtId+'">'+result[i].districtName+'</option>';
                        
                        
                    }
                    
                    //alert(htm);
                    $('#district').html('');
                    $('#district').html(htm).trigger("chosen:updated");
                    
                }
            });
            
        }


        function checkServiceType()
        {
          serviceType =  $('#serviceType').val();

          var idx = $.inArray('Others',serviceType);

           if(idx !== -1)
           {
             $('#serviceTypeOtherDiv').css('display','block');
             $('#serviceTypeOther').prop('required',true);
              
           }else{
            $('#serviceTypeOtherDiv').css('display','none');

             $('#serviceTypeOther').prop('required',false);
           } 
        }

      function checkAddressExternal()
      {
         var addressExternal = $('#incidenceAddressExternal').val();

         if(addressExternal == 'Others')
         {
         	 $('#addressExternalOtherDiv').css('display','block');
             $('#addressExternalOther').prop('required',true);
           
         }else{
              $('#addressExternalOtherDiv').css('display','none');
             $('#addressExternalOther').prop('required',false);
         }	
      }  
</script>		
		
