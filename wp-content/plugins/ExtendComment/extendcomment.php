<?php
/*
Plugin Name: Extend Comment
Version: 1.0
Plugin URI: 
Description: A plug-in to add additional fields in the comment form.
Author: 
Author URI:
*/

// Add custom meta (ratings) fields to the default comment form
// Default comment form includes name, email and URL
// Default comment form elements are hidden when user is logged in


add_filter('comment_form_default_fields','custom_fields');
function custom_fields($fields) {

		$commenter = wp_get_current_commenter();
		//$req = get_option( 'require_name_email' );
		//$aria_req = ( $req ? " aria-required='true'" : '' );

		/*$fields[ 'comment_field' ] = '<p class="comment-form-comment">'.
			'<label for="comment">' . __( 'Write your comment' ) . '</label> <span class="required">*</span>'.		
			'<textarea id="comment" name="comment" cols="45" rows="8" value="'. esc_attr( $commenter['comment_comment'] ) . 
			'" maxlength="65525" tabindex="0" aria-required="true"></textarea>';
		*/
		$fields[ 'comment_notes_after' ] = '<p class="comment-notes-after">'.
			'<span id="email-notes">Your Email or Phone is required (<span class="required">*</span>). Neither will be published on the website as per our Privacy Policy.</span>
			</p>';
		
		
		$fields[ 'author' ] = '<p class="comment-form-author">
		<span id="name-notes" style="display: block; margin-bottom: 10px;">Your Name would be visible on the website.</span>'.
			'<label for="author">' . __( 'Name' ) . '</label> <span class="required">*</span>'.
			'<input id="author" name="author" type="text" value="'. esc_attr( $commenter['comment_author'] ) . 
			'" size="30" tabindex="1" aria-required="true" /></p>';
		
		$fields[ 'email' ] = '<p class="comment-form-email">'.
			'<label for="email">' . __( 'Email' ) . '</label>'.
			'<input id="email" name="email" type="text" value="'. esc_attr( $commenter['comment_author_email'] ) . 
			'" size="30"  tabindex="2" aria-required="true" /></p>';			

		$fields[ 'phone' ] = '<p class="comment-form-phone">'.
			'<label for="phone">' . __( 'Phone' ) . '</label>'.
			'<input id="phone" name="phone" type="text" size="30"  tabindex="3" /></p>';
		
		$fields[ 'url' ] = '<p class="comment-form-url">'.
			'<label for="url">' . __( 'Website' ) . '</label>'.
			'<input id="url" name="url" type="text" value="'. esc_attr( $commenter['comment_author_url'] ) . 
			'" size="30"  tabindex="4" /></p>';


				$fields[ 'comment_notes_after' ] = '<p style="order:7;">'.
			'<span id="email-notes">Note : On approval of your comment by our Web Administrator, your name will be displayed along with your comment on the website.</span>
			</p>';

	return $fields;
}


// Save the comment meta data along with comment

add_action( 'comment_post', 'save_comment_meta_data' );
function save_comment_meta_data( $comment_id ) {
	if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') )
	$phone = wp_filter_nohtml_kses($_POST['phone']);
	add_comment_meta( $comment_id, 'phone', $phone );
}


//Add an edit option in comment edit screen  

add_action( 'add_meta_boxes_comment', 'extend_comment_add_meta_box' );
function extend_comment_add_meta_box() {
    add_meta_box( 'title', __( 'Comment Metadata' ), 'extend_comment_meta_box', 'comment', 'normal', 'high' );
}
 
function extend_comment_meta_box ( $comment ) {
    $phone = get_comment_meta( $comment->comment_ID, 'phone', true );
    wp_nonce_field( 'extend_comment_update', 'extend_comment_update', false );
    ?>
    <p>
        <label for="phone"><?php _e( 'Phone' ); ?></label>
        <input type="text" name="phone" value="<?php echo esc_attr( $phone ); ?>" class="widefat" />
    </p>   




    
    <?php
}

// Update comment meta data from comment edit screen 

add_action( 'edit_comment', 'extend_comment_edit_metafields' );
function extend_comment_edit_metafields( $comment_id ) {
    if( ! isset( $_POST['extend_comment_update'] ) || ! wp_verify_nonce( $_POST['extend_comment_update'], 'extend_comment_update' ) ) return;

	if ( ( isset( $_POST['phone'] ) ) && ( $_POST['phone'] != '') ) : 
	$phone = wp_filter_nohtml_kses($_POST['phone']);
	update_comment_meta( $comment_id, 'phone', $phone );
	else :
	delete_comment_meta( $comment_id, 'phone');
	endif;	
}

// Add the comment meta (saved earlier) to the comment text 
// You can also output the comment meta values directly in comments template  

/*
add_filter( 'comment_text', 'modify_comment');
function modify_comment( $text ){

	$plugin_url_path = WP_PLUGIN_URL;

	if( $commenttitle = get_comment_meta( get_comment_ID(), 'title', true ) ) {
		$commenttitle = '<strong>' . esc_attr( $commenttitle ) . '</strong><br/>';
		$text = $commenttitle . $text;
	} 

	if( $commentrating = get_comment_meta( get_comment_ID(), 'rating', true ) ) {
		$commentrating = '<p class="comment-rating">	<img src="'. $plugin_url_path .
		'/ExtendComment/images/'. $commentrating . 'star.gif"/><br/>Rating: <strong>'. $commentrating .' / 5</strong></p>';
		$text = $text . $commentrating;
		return $text;		
	} else {
		return $text;		
	}	 
}
*/