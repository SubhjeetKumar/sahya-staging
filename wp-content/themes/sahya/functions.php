<?php

define ('THEME_NAME',		'Sahifa' );
define ('THEME_FOLDER',		'sahifa' );
define ('THEME_VER',		'5.3.0'  );	//DB Theme Version

define( 'NOTIFIER_XML_FILE', 		"http://themes.tielabs.com/xml/".THEME_FOLDER.".xml" );
define( 'NOTIFIER_CHANGELOG_URL', 	"http://tielabs.com/changelogs/?id=2819356" );
define( 'DOCUMENTATION_URL', 		"http://themes.tielabs.com/docs/".THEME_FOLDER );

if ( ! isset( $content_width ) ) $content_width = 618;

// Main Functions
require_once ( get_template_directory() . '/framework/functions/theme-functions.php');
require_once ( get_template_directory() . '/framework/functions/common-scripts.php' );
require_once ( get_template_directory() . '/framework/functions/mega-menus.php'     );
require_once ( get_template_directory() . '/framework/functions/pagenavi.php'       );
require_once ( get_template_directory() . '/framework/functions/breadcrumbs.php'    );
require_once ( get_template_directory() . '/framework/functions/tie-views.php'      );
require_once ( get_template_directory() . '/framework/functions/translation.php'    );
require_once ( get_template_directory() . '/framework/widgets.php'                  );
require_once ( get_template_directory() . '/framework/admin/framework-admin.php'    );
require_once ( get_template_directory() . '/framework/shortcodes/shortcodes.php'    );

if( tie_get_option( 'live_search' ) )
	require_once ( get_template_directory() . '/framework/functions/search-live.php');

if( !tie_get_option( 'disable_arqam_lite' ) )
	require_once ( get_template_directory() . '/framework/functions/arqam-lite.php');


function register_my_session() {
    if( !session_id() ) {
        session_start();
    }
}
add_action('init', 'register_my_session');

function comment_validation_init() {
    //if(is_single() && comments_open() ) { 
    if( comments_open() ) { 
   ?>        
    <script type="text/javascript" src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.9/jquery.validate.min.js"></script>
    <script type="text/javascript">
    jQuery(document).ready(function($) {
    $('#commentform').validate({

    rules: {
      author: {
        required: true,
        minlength: 2
      },

      /* email: {
        required: true,
        email: true
      },	 
	*/	
	  email: {
		  required: function() {
                        //returns true if phone is empty   
                        return !$("#phone").val();
                    }
        },
	 phone: {
		 number: true,
		 minlength: 10,
		 required: function() {
                        //returns true if email is empty
                        return !$("#email").val();
                     }
				
		},
      comment: {
        required: true,
      }
    },

    messages: {
      author: "Please fill the required field",
	  email: "Email is required if no phone number is given. <br>Please fill valid email address.",
	  phone: "A phone number is required if no email is given. <br>Please fill valid phone number.",
      comment: "Please fill the required field"
    },

    errorElement: "div",
    errorPlacement: function(error, element) {
      element.after(error);
    }

    });
    });
    </script>
    <?php
    }
    }
add_action('wp_footer', 'comment_validation_init');


######### Change Leave a Reply to Leave a Comment ######
//change text to leave a reply on comment form
function ia_comment_reform ($arg) {
$arg['title_reply'] = __('Leave a Comment');
return $arg;
}
add_filter('comment_form_defaults','ia_comment_reform');
	
